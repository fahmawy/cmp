<?php 

  if(!isset($_SESSION)){
      session_start();
  }
  include_once("config.php");
  include_once("function.php");
 
  	if(isset($_SESSION['Student']))
  	{

  		header("Location: student.php");
  	}
  	
  	elseif(isset($_SESSION['Doctor']))
  	{
  		header("Location: doctor.php");
  	}
  
?>
<!DOCTYPE html>
<html lang="en-us">
<meta charset="utf-8" />
<head>
<title>Welcome To CMP | Please Login </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="modules/assests/css/style.css">
</head>
<body>
<div class="form">
<div class="header"><h2>Sign In</h2></div>
	<div class="login">
		<form action="login_cheeck.php" method="post">
			<ul>
			<li>
			<span class="un"><i class="fa fa-user"></i></span><input type="text" name="user_name" required class="text" placeholder="Enter Your Email"/></li>
			<li>
			<span class="un"><i class="fa fa-lock"></i></span><input type="password" name="user_pass" required class="text" placeholder="User Password"/></li>
			<li>
			<input name="submit" type="submit" value="LOGIN" class="btn">
			</li>
			<li><div class="span"><span class="ch"><input type="checkbox" id="r"> <label for="r">Remember Me</label> </span> <span class="ch"><a href="#">Forgot Password?</a></span></div></li>
			</ul>
		</form>
	<!-- <div class="social">
	<a href="#"><div class="fb"><i class="fa fa-facebook-f"></i> &nbsp; Login With Facebook</div></a>
	<a href="#"><div class="tw"><i class="fa fa-twitter"></i> &nbsp;  Login With Twitter</div></a>
	</div> -->
	</div>
	<div class="sign">
		<div class="need">Need new account ?</div>
		<div class="up"><a href="register.php">Sign Up</a></div>
	</div>
</div>
</body>
</html>