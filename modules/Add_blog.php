<?php 

  if(!isset($_SESSION)){
      session_start();
  }

  include_once("../config.php");
  include_once("../function.php");
  if (isset($_GET['courseCode'])) 
  $code = $_GET['courseCode'];

  if(!isset($_SESSION['Doctor']) AND !isset($_SESSION['Student']))
  { 
      header("Location: /index.php");
  }

  if(isset($_SESSION['Doctor'])){
    $UserName = $_SESSION["Doctor"]["UserName"];
    $image = $_SESSION["Doctor"]["Image"];

  }
  else{
    $UserName = $_SESSION["Student"]["UserName"];
    $image = $_SESSION["Student"]["Image"];

  }

?>
<!DOCTYPE html>
<html>
  <?php include('../header.php');?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>C</b>MP</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>CMP</b> Notifier</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
            <?php include('../custom-nav_bar.php');?>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="/modules/assests/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $UserName;?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <?php  include ('../menu.php'); ?>    
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Course</li>
          </ol>
        </section>

        <!-- Main content -->
      <section class="content"> 
      <br>
      <div class="row">
        <div class="col-md-6">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Add Article</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">
               <?php
              // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        else
        {
      $TitleErr = $ContentErr = "" ;
      if(isset($_SESSION['Doctor']))
        $userID = $_SESSION['Doctor']['TeachingID'];
      else
        $userID = $_SESSION['Student']['StudentID'];
      if ($_SERVER["REQUEST_METHOD"] == "POST" ) {
        $title=$_POST['title'];
        $acontent = $_POST['content'];
        if (empty($title))
          $TitleErr = "Add the title please";
        else
          $TitleErr ="";
        if (empty($acontent))
          $ContentErr = "Add the content please";
        else
          $ContentErr ="";
        if ($TitleErr =="" && $ContentErr == ""){
          if (!$conn)
              die('Could not connect: ' . mysql_error());
            else{
              $query = mysqli_query($conn,"INSERT INTO `cmp-notifier`.`blog` (`AuthorID`,`Content`, `Title`, `IsAccepted`) VALUES ('$userID', '$acontent', '$title', '1')");
              if ($query)
                $done = "Inserted successfully";
              else
                $done = "Error. Try Again Please";
            }
        }
      }
    }
    ?>
    <form method="post">
      <input class="form-control" type="text" class="text" name='title' placeholder='Topic Title' value="<?php if (isset($title)) echo $title?>"><?php if (isset($title)) echo $TitleErr ?><br><br>
      <textarea  class="form-control" name='content' class="text" placeholder='Content' ><?php if (isset($acontent)) echo $acontent?></textarea><?php if (isset($content)) echo $ContentErr ?><br><br>
      <input class="btn btn-block btn-success btn-flat" type="submit" value="Add Article">
      <?php if (isset($done)) echo $done ?>
    </form>
            </div>

          </div>

        </div>



      </div>     


      <?php
             
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        else
        {
           echo "Connected successfully"; 
           
           
        } 

       ?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; CMP-Notifier 2015-2016 <a href="#">CMP Notifier</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>         
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->



          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

  <?php include('../scripts.php');?>    
  </body>
</html>
