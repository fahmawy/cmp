<?php  ///////// need edit to work with REP

  if(!isset($_SESSION)){
      session_start();
  }

  include_once("../config.php");
  include_once("../function.php");

  if(!(isset($_SESSION['Doctor'])||$_SESSION['Student']['IsRep'] == 1))
  { 
      header("Location: /index.php");
  }
  if(isset($_SESSION['Student']))
  {
    $uid = $_SESSION['Student']['StudentID'];
    $username = $_SESSION['Student']['UserName'];
    $image = $_SESSION['Student']['Image'];
  }
  elseif(isset($_SESSION['Doctor']))
  {
    $uid = $_SESSION['Doctor']['TeachingID'];
    $username = $_SESSION['Doctor']['UserName'];
    $image = $_SESSION['Doctor']['Image'];
  }

?>
<!DOCTYPE html>
<html>
  <?php include('../header.php');?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>C</b>MP</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>CMP</b> Notifier</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
            <?php include('../custom-nav_bar.php');?>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo $image; ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $username;?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
          <?php  include ('../menu.php'); ?>               
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Modules > add anouncement</li>
          </ol>
        </section>

        <!-- Main content -->
      <section class="content"> 
      <br>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Add Anouncement</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

  <div class="box-body" style="display: block;">
	<form method="POST">
		<textarea class="form-control" rows='3' required name="content" placeholder="Content"></textarea> <br>
			<?php
        if(isset($_SESSION['Doctor']))
            { 

              $UserID =  $_SESSION['Doctor']['TeachingID'];   
              $Course = "SELECT * FROM users_courses WHERE UserID = '$UserID'";
              $All_Course = $conn->query($Course);
              if ($All_Course->num_rows > 0):?>
        <label for="exampleInputEmail1">Reciver</label>
        <select class="form-control" name="target">
        <option value="">-</option>
              <?php
                while($row = $All_Course->fetch_assoc()):?>
                  <?php 
                  $code = $row['CourseCode'];
                  $get_name = "SELECT courseName,year FROM courses WHERE courseCode = '$code'";
                  $Course_name = mysqli_fetch_row($conn->query($get_name));  
                  echo "<option value=".$Course_name[1].">"; 
                    echo $Course_name[0]." Students";
                  ?>  
                  </option>
                <?php
                endwhile;
                endif;
          }
          ?>
		</select><br>

		<input class="btn btn-block btn-success btn-flat" type="submit" value="Submit" name="submit">

            		<?php
	if (isset($_POST['submit'])){
		include('../config.php');
		$content = $_POST['content'];
    if(isset($_SESSION['Doctor']))
      $reciever = $_POST['target'];
    else
      $reciever = $_SESSION['Student']['Year'];
		//add to announcement table
		$insert = mysqli_query($conn,"insert into announcements (SenderID,IsShared,Type,Content,Reciever) values ($uid,true,'general','$content',$reciever)");
		echo mysqli_error($conn);
    $AnnounceID = mysqli_insert_id($conn);
		//add the target to the unseen table
		$targent = mysqli_query($conn,"select StudentID from student where Year = '$reciever'");
		if($targent){
			$unseeninsert = "insert into not_seen_announce (userID,AnnounceID) values ";
			while ($row=mysqli_fetch_assoc($targent)){
				$StudentID = $row['StudentID'];
				$unseeninsert .= "($StudentID,$AnnounceID),";
			}
			$unseeninsert = substr($unseeninsert,0,-1);
			$unseeninsert .=";";
			$query = mysqli_query($conn,$unseeninsert);
			
		}
		else
			echo "<script>window.alert('error while inserting the announcement');</script>";
	}	
?>
            </div>
          </div>
        </div>
      </div>     
      </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; CMP-Notifier 2015-2016 <a href="#">CMP Notifier</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>         
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->
          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

  <?php include('../scripts.php');?>    
  </body>
</html>
