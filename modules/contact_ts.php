<?php 

  if(!isset($_SESSION)){
      session_start();
  }

  include_once("../config.php");
  include_once("../function.php");
  if (isset($_GET['courseCode'])) 
  $code = $_GET['courseCode'];

  if(!isset($_SESSION['Student']))
  { 
      header("Location: /index.php");
  }

?>
<!DOCTYPE html>
<html>
  <?php include('../header.php');?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>C</b>MP</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>CMP</b> Notifier</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <?php include('../custom-nav_bar.php');?>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="/modules/assests/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION["Student"]["UserName"];?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
          <?php  include ('../menu.php'); ?>               
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Course</li>
          </ol>
        </section>

        <!-- Main content -->
      <section class="content"> 
      <br>
      <div class="row">
              <div class="col-md-12">
    <div class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Contact Teaching Staff</h3>
      <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
            </div>

        </div>

        <div class="box-body" style="display: block;">
             <?php

$code ="";
$year = $_SESSION['Student']['Year']; //Student's year from session
if (isset($_GET['courseCode']))
  $code = $_GET['courseCode'];
$sql = "SELECT Name,Email,PhoneNumber,IsDoctor FROM user,users_courses,teaching_staff left join users_phones on TeachingID = users_phones.UserID WHERE user.UserID = TeachingID";
$sql.="  AND users_courses.UserID = user.UserID AND users_courses.courseCode ='$code'";
$result= mysqli_query($conn, $sql);
?>


<table class="table table-bordered">

    <tr>
    <th>Name</th>
    <th>Email</th>
    <th>Phone Number</th>
    </tr>
    <?php
    while ($row= mysqli_fetch_assoc($result))
      {?>
        <tr>
        <td>
          <?php
            if($row['IsDoctor'] == 1)
              echo"Dr. ".$row['Name'];
            else
              echo"Eng. ".$row['Name'];
          ?>
        </td>
        <td>
          <?php
            echo $row['Email'];
          ?>
        </td>
        <td>
          <?php
            echo $row['PhoneNumber'];
          ?>
        </td>
        </tr> 
<?php
      }

?>
  </table> 
        </div>
    </div>
</div>


      <?php
             
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        

       ?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; CMP-Notifier 2015-2016 <a href="#">CMP Notifier</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>         
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->



          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

  <?php include('../scripts.php');?>    


  <div id="Message_modal" class="modal fade modal-success">
          <div class="modal-dialog">
          <form action="send.php" method="POST">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Send Message</h4>
              </div>
              <div id="form_b" class="modal-body">
                      <textarea id="content" class="form-control" cols='6' rows='3' required name="content" placeholder="Content"></textarea> <br>
                      <input class="btn btn-block btn-warning btn-flat" type="submit" value="Send Message" name="submit">
                      <input type="hidden" name="uid" value='' id="sid">
              </div>

            </div>
            <!-- /.modal-content -->
            </form>
          </div>
          <!-- /.modal-dialog -->
        </div>


  </body>
</html>







