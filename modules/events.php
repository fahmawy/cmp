<?php 

  if(!isset($_SESSION)){
      session_start();
  }

  include_once("../config.php");
  include_once("../function.php");
  if (isset($_GET['courseCode'])) 
  $code = $_GET['courseCode'];

  if(!isset($_SESSION['Doctor']) AND !isset($_SESSION['Student']))
  { 
      header("Location: /index.php");
  }

  if(isset($_SESSION['Doctor'])){
    $UserName = $_SESSION["Doctor"]["UserName"];
    $image = $_SESSION["Doctor"]["Image"];
    $canadd = true;
  }
  else{
    $UserName = $_SESSION["Student"]["UserName"];
    $image = $_SESSION["Student"]["Image"];
    $canadd = $_SESSION["Student"]["IsRep"];
  }

?>
<!DOCTYPE html>
<html>
  <?php include('../header.php');?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>C</b>MP</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>CMP</b> Notifier</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <?php include('../custom-nav_bar.php');?>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo $image; ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $UserName;?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
        <?php  include ('../menu.php'); ?>    
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Course</li>
          </ol>
        </section>

        <!-- Main content -->
      <section class="content"> 
      <br>
      <div class="row">
        <div class="col-md-<?php if($canadd) echo "6"; else echo "12";  ?>">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"> Comming Events</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">
       <?php
		if(isset($_SESSION['Doctor']))
			$userID = $_SESSION['Doctor']['TeachingID'];
		else
			$userID = $_SESSION['Student']['StudentID'];					 
	  		$query = mysqli_query($conn,"SELECT IsStudent FROM user WHERE UserID = $userID");

	  		if($query){
	  			if(mysqli_num_rows($query) > 0){ 
		  			$row = mysqli_fetch_array($query);
		  			if($row['IsStudent'] == 0){
		  				$select = "SELECT `events`.`Title`,`events`.`EventID`,`events`.`IsConfirmed`, `user`.`Name`, `events`.`Target` FROM events, user WHERE `events`.`CreatorID`= `user`.`UserID`";
		  			}else{
		  				$select = "SELECT `events`.`Title`,`events`.`EventID`,`events`.`IsConfirmed`, `events`.`Target`, `user`.`Name` FROM events, user,student WHERE `events`.`CreatorID`= `user`.`UserID` and `student`.`StudentID` = $UserID and (`events`.`Target` = 5 or `events`.`Target`= `student`.`Year`)";
		  			}
		  			$query = mysqli_query($conn,$select);
	  				if(mysqli_num_rows($query) > 0){
		  				echo "<h1> Those are the events you are invited to </h1>";
			  			while($row = mysqli_fetch_array($query))
						{
							if( $row['IsConfirmed'] == 1)
								$print = "<div class='event' style='border-bottom: 1px dashed black'>
									  <a href='view-events.php?EventID=" . $row['EventID'] . " '>
									  	<h2 id='head'>" . $row['Title'] . "</a>    " ;
								if($row['Target'] == 5)
									$print .= "General";
								else
									$print .= "CMP " .$row['Target'];
								$print .= "</h2>
									  <h3 id='creator'> By : " . $row['Name'] ." </h3>
									  </div>";
								echo $print;
						}
					}else
	  					echo "You aren't invited to any Events";
				}else
					echo "<h1>Wrong ID</h1>";
				
	  		}else
	  			echo "<h1>Error Has Occured</h1>";
	  	
		?>
            </div>

          </div>

        </div>
        <?php if($canadd){  ?>
        <div class="col-md-6">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Add Event</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">
              <?php
    $DayErr = $TimeErr = $placeErr = $desErr = $titleErr =  "" ;
    if(isset($_SESSION['Doctor']))
      $userID = $_SESSION['Doctor']['TeachingID'];
    else
      $userID = $_SESSION['Student']['StudentID'];
    if ($_SERVER["REQUEST_METHOD"] == "POST" ) {
      $place=$_POST['Place'];
      $Day = $_POST['eventDay'];
      $Time = $_POST['eventTime'];
      $des = $_POST['Description'];
      $reciever = $_POST['reciever'];
      $title = $_POST['title'];
      if (empty($title))
        $titleErr = "Add the Title please";
      else
        $titleErr = "";
      //Place Check
      if (empty($place))
        $placeErr = "Add the place please";
      else
        $placeErr = "";

      //Description Check
      if (empty($des))
        $desErr = "Add the Description please";
      else
        $desErr ="";

      //Date Check
      if (empty($Day))
        $DayErr = "Determine the day please";
      else{
        $DayErr = "";
      }
      //Time Check
      if (empty($Time))
        $TimeErr = "Determine the Time please";
      else
        $TimeErr ="";
      if ($TimeErr == "" && $DayErr == "") {
        $timestamp = strtotime("$Day $Time");
        if ($timestamp - time() >= 25*60*60)  //difference is at least one day (it reads the time one hour earlier than it's on the screen)  
        {
          if ($DayErr == "" && $TimeErr == "" && $placeErr == "" && $desErr == "" && $titleErr == "" ){
            $con = mysqli_connect("localhost","root","","cmp-notifier");
            if (!$con)
                die('Could not connect: ' . mysql_error());
              else{
                //insert the event
                $query = mysqli_query($con,"INSERT INTO `events`(`CreatorID`, `Title`,`Target`, `Description`, `Place`, `Time`, `IsConfirmed`)
                                VALUES ('$userID','$title','$reciever','$des','$place','$timestamp','1')");
                $eventLastID = mysqli_insert_id($con);
                if ($query){
                  //insert the announcement
                  $AnnCon = "..\events\event.php?EventID=" . $eventLastID;
                  $query = mysqli_query($con,"INSERT INTO `announcements`(`SenderID`,`IsShared`, `Type`, `Content`, `Reciever`)
                              VALUES ('$userID','1','event','$AnnCon','$reciever')");
                  $AnnLastID = mysqli_insert_id($con);
                  if ($query){
                    if($reciever == 5)
                      $query = mysqli_query($con,"SELECT `StudentID` FROM `student`");
                    else
                      $query = mysqli_query($con,"SELECT `StudentID` FROM `student` WHERE `Year` = $reciever");
                    if($query){
                      //insert into not_seen_announce
                      $insert = 'INSERT INTO `not_seen_announce`(`UserID`, `AnnounceID`) VALUES';
                      while($row = mysqli_fetch_array($query))
                        $insert .= "($row[0],$AnnLastID), ";
                      $insert = substr($insert,0,-2);
                      $query = mysqli_query($con,$insert);
                      if($query)
                        $done = "announcement is sent";
                      else{
                        $query = mysqli_query($con,"DELETE FROM `events` where `EventID` = $eventLastID");
                        $query = mysqli_query($con,"DELETE FROM `announcements` where `AnnID` = $AnnLastID");
                        $done = "Error. Try Again Please --> not_seen";
                      }
                    }else{
                      $query = mysqli_query($con,"DELETE FROM `events` where `EventID` = $eventLastID");
                      $query = mysqli_query($con,"DELETE FROM `announcements` where `AnnID` = $AnnLastID");
                      $done = "Error. Try Again Please --> not_seen";
                    }
                  }
                  else{
                    $query = mysqli_query($con,"DELETE FROM `events` where `EventID` = $eventLastID");
                    $done = "Error. Try Again Please --> announcement";
                  }
                }
                else
                  $done = "Error. Try Again Please --> event";
              }
          }
        }
        else{
          echo "Enter a future date that's after at least 24 hours from now please";
        }
      }
      
    }
    ?>
    <form method="post">
      <input class="form-control" type="text" name='title' placeholder='Event Title' value="<?php if (isset($title)) echo $title?>">
      <?php if (isset($title)) echo $titleErr ?><br><br>
      <input class="form-control" type="text" name='Place' placeholder='Where the event is held' value="<?php if (isset($place)) echo $place?>">
      <?php if (isset($place)) echo $placeErr ?><br><br>
      <input class="form-control" type="date" name="eventDay" value="<?php if (isset($Day)) echo $Day ?>">
      <?php if (isset($Day)) echo $DayErr ?><br><br>
      <input class="form-control" type="time" name="eventTime" value="<?php if (isset($Time)) echo $Time ?>">
      <?php if (isset($Time)) echo $TimeErr ?><br><br>
      <textarea class="form-control"  name='Description' placeholder='Event Desription' ><?php if (isset($des)) echo $des?></textarea>
      <?php if (isset($des)) echo $desErr ?>
      <p> Who are the recievers ? </p>
      <select class="form-control" name="reciever" >
        <option></option>
        <option value="1" <?php if (isset($reciever) && $reciever==1) echo "selected" ?>> CMP1 </option>
        <option value="2" <?php if (isset($reciever) && $reciever==2) echo "selected" ?>> CMP2 </option>
        <option value="3" <?php if (isset($reciever) && $reciever==3) echo "selected" ?>> CMP3 </option>
        <option value="4" <?php if (isset($reciever) && $reciever==4) echo "selected" ?>> CMP4 </option>
        <option value="5" <?php if (isset($reciever) && $reciever==5) echo "selected" ?>> All Students </option>
      </select><br><br>
      <input class="btn btn-block btn-success btn-flat" type="submit" value="Add Event">
      <?php if (isset($done)) echo $done ?>
    </form>
            </div>

          </div>

        </div>
<?php } ?>

      </div>     


      <?php
             
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        else
        {
           echo "Connected successfully"; 
           
           
        } 

       ?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; CMP-Notifier 2015-2016 <a href="#">CMP Notifier</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>         
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->



          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

  <?php include('../scripts.php');?>    
  </body>
</html>
