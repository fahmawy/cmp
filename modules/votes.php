<?php 

  if(!isset($_SESSION)){
      session_start();
  }

  include_once("../config.php");
  include_once("../function.php");

  if(!isset($_SESSION['Student']) AND !isset($_SESSION['Doctor']))
  { 
      header("Location: /index.php");
  }

  if(isset($_SESSION['Doctor'])){
    $uid = $_SESSION['Doctor']['TeachingID'];
    $UserName = $_SESSION["Doctor"]["UserName"];
    $image = $_SESSION["Doctor"]["Image"];
    $canadd = true;
  }
  else{
    $uid = $_SESSION['Student']['StudentID'];
    $UserName = $_SESSION["Student"]["UserName"];
    $image = $_SESSION["Student"]["Image"];
    $year = $_SESSION["Students"]["Year"];
    $canadd = $_SESSION["Student"]["IsRep"];
  }

?>
<!DOCTYPE html>
<html>
  <?php include('../header.php');?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>C</b>MP</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>CMP</b> Notifier</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <?php include('../custom-nav_bar.php');?>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo $image; ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $UserName; ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
          <?php  include ('../menu.php'); ?>         
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Course</li>
          </ol>
        </section>

        <!-- Main content -->
      <section class="content"> 
      <br>
      <div class="row">
        <div class="col-md-<?php if($canadd) echo "6"; else echo "12";  ?>">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"> Votes </h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">
       <?php
    if(isset($_SESSION['Doctor'])){
      $userID = $_SESSION['Doctor']['TeachingID'];
      $select = "SELECT VotingID,Question,Name,Title FROM votes,user WHERE MakerID = UserID and MakerID = $userID";
    }
    else{
      $userID = $_SESSION['Student']['StudentID'];   
      $select = "SELECT VotingID,Question,Name,Title FROM votes,user WHERE MakerID = UserID and Reciever = $year";
      }        
    $query = mysqli_query($conn,$select);
    echo mysqli_error($conn);
            if(mysqli_num_rows($query) > 0){
              while($row = mysqli_fetch_array($query))
            {
                $print = "<div class='vote' style='border-bottom: 1px dashed black'>
                    <a href='vote.php?vID=" . $row['VotingID'] . " '>
                      <h2 id='head'>" . $row['Question'] . "</h2></a>    " ;
                      $print .= "<h3 id='creator'> By : " . $row['Title'] ."." . $row['Name'] ." </h3>
                    </div>";
                echo $print;
            }
          }else
              echo "You aren't target in any vote";
      
    ?>
            </div>
          </div>
        </div>

        <?php if($canadd){ ?>
        <div class="col-md-6">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Add Vote</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">
            <form method="POST">
      <label for="exampleInputEmail1">Question</label>
      <input class="form-control" type="text" name="question"><br>
      <?php
        if(isset($_SESSION['Doctor']))
            { 

              $UserID =  $_SESSION['Doctor']['TeachingID'];   
              $Course = "SELECT * FROM users_courses WHERE UserID = '$UserID'";
              $All_Course = $conn->query($Course);
              if ($All_Course->num_rows > 0):?>
        <label for="exampleInputEmail1">Reciver</label>
        <select class="form-control" name="receiver">
        <option value="">-</option>
              <?php
                while($row = $All_Course->fetch_assoc()):?>
                  <?php 
                  $code = $row['CourseCode'];
                  $get_name = "SELECT courseName,year FROM courses WHERE courseCode = '$code'";
                  $Course_name = mysqli_fetch_row($conn->query($get_name));  
                  echo "<option value=".$Course_name[1].">"; 
                    echo $Course_name[0]." Students";
                  ?>  
                  </option>
                <?php
                endwhile;
                endif;
          }
          ?>
      </select><br>
      <label for="exampleInputEmail1">Expire Date</label>
      <input class="form-control" type="date" name="ex-date"> <br>
      <label for="exampleInputEmail1">Allowed Answers</label>
      <div id="newInput">
      <p class="control-label" for="inputWarning"><i class="fa fa-bell-o"></i> Write one answer and for more answers click on "Add another answer"</p>      
            <input class="form-control" type="text" name="answers[]" size="30" placeholder="Allowed answer"> 
        </div>
        <hr>
        <div>
               <input class="btn btn-default" type="button" value="Add answer" onClick="addInput('newInput');">
               <input style="float: right;" class="btn btn-primary" type="submit" name="submit" value="Submit">                
        </div>
    </form>
  </div>
</body>
</html>
<?php
  if (isset($_POST['submit'])){
    include('../config.php');
    $questErr = $recErr = $ansErr = "" ;
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $question = $_POST['question'];
    if(isset($_SESSION['Doctor']))
      $receiver = $_POST['receiver'];
    else
      $receiver = $_SESSION['Student']['Year'];
    $answers = $_POST["answers"];
    $all_answers = implode(",", $answers);
    $ex_date = $_POST['ex-date'];
    //Question Validation
    if(empty($content))
      $contentErr = "Please enter your question!";
    else
      $contentErr ="";
    //Receiver Validation
    if(empty($receiver))
      $recErr = "Receiver is required";
    else
      $recErr ="";

    //Answers Validation
    if (empty($all_answers))
      $ansErr = "Enter At least one answer!";
    // Make sure no dupliactes are there
      $query = mysqli_query($conn, "SELECT Question FROM votes WHERE Question='$question'");
      if ($query){
        if (mysqli_num_rows($query) == 0) {
          $insert= mysqli_query($conn,"INSERT INTO votes (
                                    MakerID,
                                    Question,
                                    Expire_Date,
                                    Reciever
                                  ) VALUES (
                                    '$uid',
                                    '$question',
                                    '$ex_date',
                                    '$receiver'         
                                  )");
          $voteid = mysqli_insert_id($conn);
          $text = "INSERT INTO allowed_answers (VotingID,Allowed_Answer) VALUES ";
          foreach ($answers as $one) {
            $text .= "('$voteid','$one'),";
          }
          $text = substr($text, 0,-1);
          $text.= ";";
          $insert_ans = mysqli_query($conn,$text);

          //add the target to the unseen table and unanswer table
          if($receiver !=5)
            $targent = mysqli_query($conn,"select StudentID from student where Year = '$receiver'");
          else
            $targent = mysqli_query($conn,"select StudentID from student");
          if($targent){
            $unansinsert = "insert into not_ans_votes (userID,VotingID) values ";
            while ($row=mysqli_fetch_assoc($targent)){
              $StudentID = $row['StudentID'];
              $unansinsert .= "($StudentID,$voteid),";
            }
            $unansinsert = substr($unansinsert,0,-1);
            $unansinsert .=";";

            $query = mysqli_query($conn,$unansinsert);
          
          if(!$insert || !$insert_ans) {
            echo 'Insert Error: '.mysqli_error($conn);
            $_SESSION['msg'] = "<div class='regmsg fail'>Something went wrong, please answer again!</div>";
          } else {
            //setcookie('votes');
            $_SESSION['msg'] = "<div class='regmsg success'>Your data have successfully been sumbitted. Thanks for your time.</div>";
          }
        } else {
          $_SESSION['msg'] = "<div class='regmsg fail'>You have filled this before.</div>";
        }
      } else {
        $_SESSION['msg'] = "<div class='regmsg fail'>Something went wrong, please try again!</div>";
      }
    }
  }
}
?>
            </div>
          </div>
        </div>
      </div>     
      <?php
           }  
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }        

       ?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; CMP-Notifier 2015-2016 <a href="#">CMP Notifier</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>         
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->



          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

  <?php include('../scripts.php');?>   

    <script type="text/javascript">
    var counter = 1;
    function addInput(divName){
          var newdiv = document.createElement('div');
          newdiv.innerHTML = "<br><input id='newin' class='form-control' type='text' name='answers[]' placeholder='Allowed answer'>";
          document.getElementById(divName).appendChild(newdiv);
          counter++;
  }
  </script>


  </body>
</html>
