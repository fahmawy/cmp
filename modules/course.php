<?php 

  if(!isset($_SESSION)){
      session_start();
  }

  include_once("../config.php");
  include_once("../function.php");
  if (isset($_GET['courseCode'])) 
  $code = $_GET['courseCode'];

  if(!isset($_SESSION['Doctor']))
  { 
      header("Location: /index.php");
  }

?>
<!DOCTYPE html>
<html>
  <?php include('../header.php');?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>C</b>MP</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>CMP</b> Notifier</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <?php include('../custom-nav_bar.php');?>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo $_SESSION['Doctor']['Image'] ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION["Doctor"]["UserName"];?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
          <?php  include ('../menu.php'); ?>               
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Course</li>
          </ol>
        </section>

        <!-- Main content -->
      <section class="content"> 
      <br>
      <div class="row">
        <div class="col-md-6">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Add Reference</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">
              <form method="POST">
      Reference Name: <input class="form-control" type="text" name="refname"><br>
      Reference ISBN: <input class="form-control" type="text" name="isbn"><br>
      <input style="margin-top: 19px;"type="submit" class="btn btn-block btn-success btn-flat" >
      <?php
        $RefName = $ISBN = "";
        if (isset($_GET['courseCode'])) 
        $code = $_GET['courseCode'];

        if(empty($_POST['refname']))
          $Err = "Reference Name is required!";
        else
          $RefName = $_POST['refname'];
        if(empty($_POST['isbn']))
          $Err2 = "ISBN is required";
        else
          $ISBN = $_POST['isbn'];
        $query = mysqli_query($conn, "SELECT ReferenceISBN FROM course_references WHERE ReferenceISBN='$ISBN'");
        if ($query){
          if (mysqli_num_rows($query) == 0) {
            mysqli_set_charset($conn, 'utf8');
            $sql = mysqli_query($conn,"INSERT INTO course_references (
                                courseCode,
                                ReferenceISBN,
                                ReferenceName
                              ) VALUES (
                                '$code',
                                '$ISBN',
                                '$RefName'
                              )
            "); 
          }
          else {
            $Error = "<div class='regmsg fail'>You have filled this before.</div>";
          }
        }
      ?>
    </form>
            </div>

          </div>

        </div>


<!-- add project-->
        <div class="col-md-6">
          <div class="box box-success box-solid ">
            <div class="box-header with-border">
              <h3 class="box-title">Add Project</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">
                    <form method="POST">
      Select Year:
      <select class="form-control" name="year">
        <option disabled selected>Select Year</option>
        <option value="1">CMP 1</option>
        <option value="2">CMP 2</option>
        <option value="3">CMP 3</option>
        <option value="4">CMP 4</option>
        <option value="5">All</option>
      </select><br>
      Project Description: <textarea class="form-control" name="description"></textarea><br>
      <input class="btn btn-block btn-success btn-flat" type="submit">
      <?php
        $Year = $des = "";
        if (isset($_GET['courseCode'])) 
          $code = $_GET['courseCode'];

        if(empty($_POST['year']))
          $Err = "Year is required!";
        else
          $Year = $_POST['year'];
        if(empty($_POST['description']))
          $Err2 = "Description is required";
        else
          $des = $_POST['description'];
        $query = mysqli_query($conn, "SELECT description FROM project WHERE Description='$des'");
        if ($query){
          if (mysqli_num_rows($query) == 0) {
            mysqli_set_charset($conn, 'utf8');
            $sql = mysqli_query($conn,"INSERT INTO project (
                                courseCode,
                                Description,
                                Year
                              ) VALUES (
                                '$code',
                                '$des',
                                '$Year'
                              )
            "); }
          else {
            $Error = "<div class='regmsg fail'>You have filled this before.</div>";
          }
        }
      ?>
    </form>
            </div>

          </div>

        </div>


<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Students Data</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <?php
      if (isset($_GET['courseCode'])) {
        $code = $_GET['courseCode'];
        $sql = "SELECT Name,Email,PhoneNumber,BN,Section FROM user,users_courses,student left join users_phones on StudentID = users_phones.UserID WHERE user.UserID = StudentID AND users_courses.UserID = user.UserID AND users_courses.courseCode ='$code'";
        $query = mysqli_query($conn,$sql); //Note if there is no phone number no data viewed
        if(!$query) 
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        echo '<table class="table table-hover"><tbody><tr>';
        echo "<th>BN</th>";
        echo "<th>Section</th>";
        echo "<th>Name</th>";
        echo "<th>Email</th>";
        echo "<th>Phone</th></tr>";
        while ($row = mysqli_fetch_assoc($query)) {
          echo "<tr>";
          echo "<td>".$row['BN']."</td>";
          echo "<td>".$row['Section']."</td>";
          echo "<td>".$row['Name']."</td>";
          echo "<td>".$row['Email']."</td>";
          echo "<td>".$row['PhoneNumber']."</td>";
          echo "</tr>";
        }
        echo "</tbody></table>";
      }
     ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>


<!--Grades-->
<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Students Grades</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <?php
      if (isset($_GET['courseCode'])) {
        $code = $_GET['courseCode'];
        $sql = "SELECT user.UserID,Name,BN,Type,Value FROM user,student,grades WHERE courseCode='$code' AND student.StudentID = grades.StudentID";
        $sql.= " AND user.UserID=student.StudentID";
        $query = mysqli_query($conn,$sql);
        if(!$query) 
          "Error: " . $sql . "<br>" . mysqli_error($conn);
        echo '<table class="table table-hover"><tbody><tr>';
        echo "<th>BN</th>";
        echo "<th>Name</th>";
        echo "<th>Grade Type</th>";
        echo '<th>Value</th><th style="text-align: center;">Send Message</th></tr>';
        while ($row = mysqli_fetch_assoc($query)) {
          echo "<tr>";
          echo "<center><td>".$row['BN']."</td>";
          echo "<td>".$row['Name']."</td>";
          echo "<td>".$row['Type']."</td>";
          echo "<td>".$row['Value']."</td>";
          echo '<td><button  data-toggle="modal" data-target="#Message_modal" data-id="'.$row['UserID'].'" type="button" class="open-MessageDialog btn btn-block btn-warning btn-flat">Message</button></td>';
          echo "</tr>";
        }
        echo "</tbody></table>";
      }
     ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

        <div class="col-md-12">
          <div class="box box-success box-solid ">
            <div class="box-header with-border">
              <h3 class="box-title">Add Grades</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">
                <form  method="POST">
     
    <input class="form-control" type="text" name="grade_type" placeholder="Enter Grade Type Ex: Midterm | Quiz"><br><br>
      <table class="table table-hover"><tbody>
        <tr>
          <th>Sec</th>
          <th>BN</th>
          <th>Name</th>
          <th>Grade Value</th>
        </tr>
          
          <?php #
          if (isset($_GET['courseCode']))
            $code = $_GET['courseCode'];
          $sql = "SELECT StudentID,Name,BN,Section FROM user,student,users_courses WHERE users_courses.courseCode = '$code' AND user.UserID=student.StudentID AND users_courses.UserID = student.StudentID";
          $query = mysqli_query($conn,$sql);
          echo mysqli_error($conn);
          while ($row = mysqli_fetch_assoc($query)) {
            ?>
            <tr>
              <td><?php echo $row['Section']; ?></td>
              <td><?php echo $row['BN']; ?></td>
              <td><?php echo $row['Name']; ?></td>
              <td><input class="form-control" type="text" name="grades[<?php echo $row['StudentID']; ?>]"></td>
            </tr>
          <?php
          }
          ?>
      </tbody></table>
      <input class="btn btn-block btn-success btn-flat" type="submit" name="gradesubmit"><br>
      </form>
      <?php
        if(isset($_POST['gradesubmit'])){
          if (isset($_GET['courseCode'])) 
            $code = $_GET['courseCode'];
          // Add Grades
          if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $grade_type = $_POST['grade_type'];
            $grades = $_POST['grades'];
            //Type Validation
            if(empty($content))
              $TypeErr = "Please enter grades type";
            else
              $TypeErr ="";

            //Type dup
            $check = "SELECT * FROM grades WHERE courseCode='$code' AND Type='$grade_type'";
            $query = mysqli_query($conn,$check);
            if(mysqli_num_rows($query) != 0 )
              echo "<script> window.alert('data already exist, Edit'); </script>";
            else {
              //Insert
              $sql = "SELECT StudentID FROM user,student,users_courses WHERE users_courses.courseCode = '$code' AND user.UserID=student.StudentID AND users_courses.UserID = student.StudentID";
              $query = mysqli_query($conn,$sql);
              $add = "INSERT INTO grades (StudentID,courseCode,Type,Value) VALUES ";
              while ($row = mysqli_fetch_assoc($query))
              {
                $index = $row['StudentID'];
                $add.="('$index','$code','$grade_type','$grades[$index]'),";

              }
              $add = substr($add, 0,-1);
              $add .=";";
              mysqli_query($conn,$add);

              // send announcement to students
              $coursenameq = mysqli_query($conn,"select courseName,year from courses where courseCode = '$code'");
              $coursenameq = mysqli_fetch_assoc($coursenameq);
              $coursename = $coursenameq['courseName'];
              $uid = $_SESSION['Doctor']['TeachingID'];
              $content = $grade_type ." Grades for ".$coursename ." have been submitted";
              $reciever = $coursenameq['year'];
              //add to announcement table
              $insert = mysqli_query($conn,"insert into announcements (SenderID,IsShared,Type,Content,Reciever) values ($uid,true,'general','$content',$reciever)");
              echo mysqli_error($conn);
              $AnnounceID = mysqli_insert_id($conn);
              //add the target to the unseen table
              $targent = mysqli_query($conn,"SELECT StudentID FROM user,student,users_courses WHERE users_courses.courseCode = '$code' AND user.UserID=student.StudentID AND users_courses.UserID = student.StudentID");
              if($targent){
                $unseeninsert = "insert into not_seen_announce (userID,AnnounceID) values ";
                while ($row=mysqli_fetch_assoc($targent)){
                  $StudentID = $row['StudentID'];
                  $unseeninsert .= "($StudentID,$AnnounceID),";
                }
                $unseeninsert = substr($unseeninsert,0,-1);
                $unseeninsert .=";";
                echo $unseeninsert;
                $query = mysqli_query($conn,$unseeninsert);
                echo mysqli_error($conn);
              }
            }
        }
      }
      ?>
            </div>

          </div>

        </div>


                <div class="col-md-12">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Contact Representive</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">
              <form method="POST">
                <textarea class="form-control" cols='6' rows='3' required name="content" placeholder="Content"></textarea> <br>
                <input class="btn btn-block btn-success btn-flat" type="submit" value="Submit" name="submitcon">
              </form>

<?php
  if (isset($_POST['submitcon'])){

    $uid = $_SESSION['Doctor']['TeachingID'];
    $content = $_POST['content'];
    $coursecode = $_GET['courseCode'];

    $query = mysqli_query($conn,"select StudentID from student,users_courses where UserID = StudentID and CourseCode = '$coursecode' and IsRep = 1");
    $query = mysqli_fetch_assoc($query);
    $reciever = $query['StudentID'];
    //add to announcement table
    $insert = mysqli_query($conn,"INSERT INTO announcements (SenderID,IsShared,Type,Content,Reciever) values ($uid,false,'message','$content','$reciever')");
    //add the target to the unseen table
    if($insert){
      $AnnounceID = mysqli_insert_id($conn);
      $unseeninsert = "insert into not_seen_announce (userID,AnnounceID) values ($reciever,$AnnounceID)";
      $query = mysqli_query($conn,$unseeninsert);
    }
    else{
      echo mysqli_error($conn);
      echo "<script>window.alert('error while sending the message');</script>";
    }
  } 
?>
            </div>

          </div>

        </div>


      </div>     


      <?php
             
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        else
        {
           echo "Connected successfully"; 
           
           
        } 

       ?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; CMP-Notifier 2015-2016 <a href="#">CMP Notifier</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>         
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->



          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

  <?php include('../scripts.php');?>    


  <div id="Message_modal" class="modal fade modal-success">
          <div class="modal-dialog">
          <form action="send.php" method="POST">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Send Message</h4>
              </div>
              <div id="form_b" class="modal-body">
                      <textarea id="content" class="form-control" cols='6' rows='3' required name="content" placeholder="Content"></textarea> <br>
                      <input class="btn btn-block btn-warning btn-flat" type="submit" value="Send Message" name="submit">
                      <input type="hidden" name="uid" value='' id="sid">
              </div>

            </div>
            <!-- /.modal-content -->
            </form>
          </div>
          <!-- /.modal-dialog -->
        </div>


  </body>
</html>

<script>
$(document).on("click", ".open-MessageDialog", function() {
     $('#sid').val($(this).data('id'));

});
</script>