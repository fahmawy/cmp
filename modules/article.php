<?php 

  if(!isset($_SESSION)){
      session_start();
  }

  include_once("../config.php");
  include_once("../function.php");
  if(!isset($_SESSION['Doctor']) AND !isset($_SESSION['Student']))
  { 
      header("Location: /index.php");
  }

?>
<!DOCTYPE html>
<html>
  <?php include('../header.php');
  mysqli_set_charset($conn, 'UTF8');
  ?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>C</b>MP</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>CMP</b> Notifier</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
            <?php include('../custom-nav_bar.php');?>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
            <?php

            if(isset($_SESSION['Student']))
            {
            		$image = $_SESSION['Student']['Image'];
            }
            elseif(isset($_SESSION['Doctor']))
            {
            	   $image = $_SESSION['Doctor']['Image'];
            }
            
            ?>
              <img src="<?php echo $image;?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">

                        <?php

            if(isset($_SESSION['Student']))
            {
            		$name = $_SESSION['Student']['UserName'];
            }
            elseif(isset($_SESSION['Doctor']))
            {
            	   $name = $_SESSION['Doctor']['UserName'];
            }
            
            ?>


              <p><?php echo $_SESSION["Doctor"]["UserName"];?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <?php  include ('../menu.php'); ?>          
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
<!--         
        <section class="content-header">
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Course</li>
          </ol>
        </section> -->

        <!-- Main content -->
      <section class="content"> 
      <br>
      <div class="row">

              		<?php
                  		$ArticleID = $_GET['ArticleID'];

              	  		$query = mysqli_query($conn,"SELECT `blog`.`Title`,`blog`.`Image`,`blog`.`Content`, `user`.`Name` FROM blog, user WHERE `blog`.`ArticleID` = $ArticleID and `blog`.`AuthorID`= `user`.`UserID`");
              	  		$row = mysqli_fetch_array($query);
                      mysqli_set_charset($conn, 'UTF8');                  	  	
                  ?>

          <div class="col-md-12">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $row['Title'];?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">
            <div class="col-md-12">
              <img style="width:100%; height:400px; margin-bottom:30px;" src="<?php echo $row['Image']?>"/>
            </div>
            <p style="font-size: 20px;text-align: justify;padding: 20px;"><?php echo $row['Content']?></p>


    


            </div>

          </div>

        </div>



      </div>     


      <?php
             
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

       ?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; CMP-Notifier 2015-2016 <a href="#">CMP Notifier</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>         
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->



          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

  <?php include('../scripts.php');?>    
  </body>
</html>
