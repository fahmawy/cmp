<?php 

  if(!isset($_SESSION)){
      session_start();
  }

  include_once("../config.php");
  include_once("../function.php");

  if(!isset($_SESSION['Student']))
  { 
      header("Location: /index.php");
  }

?>
<!DOCTYPE html>
<html>
  <?php include('../header.php');?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>C</b>MP</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>CMP</b> Notifier</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
            <?php include('../custom-nav_bar.php');?>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo $_SESSION["Student"]["Image"];?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION["Student"]["UserName"];?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
          <?php  include ('../menu.php'); ?>        
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Student Course</li>
          </ol>
        </section>

        <!-- Main content -->
      <section class="content"> 
      <br>
      <div class="row">

      <!-- Box Content-->
        <div class="col-md-12">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Instructors</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">
    <?php
      if (isset($_GET['courseCode'])) {
        $code = $_GET['courseCode'];
        $sql = "SELECT Name,Email,IsDoctor,TeachingStaffID,Day,FromTime,ToTime FROM user,users_courses,teaching_staff ";
        $sql.=" left join teaching_staff_oh on TeachingID = TeachingStaffID WHERE courseCode='$code' AND user.UserID=users_courses.UserID AND users_courses.UserID=TeachingID";
        $sql.=" AND user.UserID = TeachingID order by IsDoctor desc";
        $query = mysqli_query($conn,$sql);
        if(!$query) 
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        ?>
        <table class="table table-bordered">
        <tr>
        <th rowspan='2'>Name</th>
        <th rowspan='2'>Email</th>
        <th colspan='3'>Office hours</th>
        </tr>
        <tr>
          <td>Day</td>
          <td>From</td>
          <td>To</td>
        </tr>
        <?php
        while ($row = mysqli_fetch_assoc($query)) 
        {
          echo "<tr>";
          if($row['IsDoctor'] == 1)
            echo "<td>Dr. ".$row['Name']."</td>";
          else
            echo "<td>Eng. ".$row['Name']."</td>";

          echo "<td>".$row['Email']."</td>";
          echo "<td>".$row['Day']."</td>";
          echo "<td>".$row['FromTime']."</td>";
          echo "<td>".$row['ToTime']."</td>";


          echo "</tr>";
        }
          echo "</table>";
      }
     ?>
      
            </div>

          </div>

        </div>
    <!-- Box End-->



      <!-- Box Content-->
        <div class="col-md-12">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Course Schedule</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">
    <?php
      if (isset($_GET['courseCode'])) {
        $code = $_GET['courseCode'];
        $sql = "SELECT Day,LectureNum,IsSection,Alternating FROM course_schedule WHERE courseCode='$code'";
        $query = mysqli_query($conn,$sql);
        if(!$query) 
          echo "Error: " . $sql . "<br>" . mysqli_error($conn);?>

        <table class="table table-bordered">
        <th>Day</th>
        <th>Lecture No.</th>
        <th>Type</th>
        <th>Alternating</th>
        <?php
        while ($row = mysqli_fetch_assoc($query)) {
          echo "<tr>";
          echo "<td>".$row['Day']."</td>";
          echo "<td>".$row['LectureNum']."</td>";
          if($row['IsSection'] == 1) {
            echo "<td>Section 1</td>";
          }
          else if ($row['IsSection'] == 2) {
            echo "<td>Section 2</td>";
          }
          else if ($row['IsSection'] == 3) {
            echo "<td>Section 1,2</td>";
          }
          else {
            echo "<td>Lecture</td>";
          }
          if($row['Alternating'] == 1)
            echo "<td>Yes</td>";
          else
            echo "<td>No</td>";
          echo "</tr>";
        }
        echo "</table>";
      }
     ?>
            </div>

          </div>

        </div>
    <!-- Box End-->


      <!-- Box Content-->
        <div class="col-md-6">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Course Tools</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">
              
                  <?php
        
                  if (isset($_GET['courseCode'])) {
                    $code = $_GET['courseCode'];
                    $sql = "SELECT drive  FROM courses WHERE courseCode='$code'";
                    $query = mysqli_query($conn,$sql);
                    if(!$query) 
                      "Error: " . $sql . "<br>" . mysqli_error($conn);

                    while ($row = mysqli_fetch_assoc($query)) {
                      echo "<a style='width: 134px; height:100px;' class='btn btn-app' href='".$row['drive']."' target='_blank'><i style='font-size: 50px;' class='fa fa-inbox'></i>Drive link</a>";
                    }
                  }
                 ?>


              <?php if($_SESSION['Student']['IsRep']){
                  $code = $_GET['courseCode'];
                ?>

              <a  style="width: 134px; height:100px;" class="btn btn-app" href="/modules/contact_ts.php?courseCode=<?php echo $code;?>">                 
                <i style='font-size: 50px;' class="fa fa-envelope"></i> Contact TS
              </a>
              <?php }?>
            </div>

          </div>

        </div>
    <!-- Box End-->



      <!-- Box Content-->
        <div class="col-md-6">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">References</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">
    <?php
      if (isset($_GET['courseCode'])) {
        $code = $_GET['courseCode'];
        $sql = "SELECT ReferenceName FROM course_references WHERE courseCode='$code'";
        $query = mysqli_query($conn,$sql);
        if(!$query) 
          "Error: " . $sql . "<br>" . mysqli_error($conn);
        
        while ($row = mysqli_fetch_assoc($query)) {
          echo "<ul>";
          echo "<li>".$row['ReferenceName']."</li>";
          echo "</ul>";
        }
      }
     ?>
            </div>

          </div>

        </div>
    <!-- Box End-->



      <!-- Box Content-->
        <div class="col-md-6">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Grades</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">
            <?php
          $UserID = $_SESSION['Student']['StudentID'];  
          if (isset($_GET['courseCode'])) {
            $code = $_GET['courseCode'];
            $sql = "SELECT Type,Value  FROM grades WHERE courseCode='$code' AND StudentID='$UserID'";
            $query = mysqli_query($conn,$sql);
            if(!$query) 
              "Error: " . $sql . "<br>" . mysqli_error($conn);?>
            
            <table class="table table-bordered">
            <tr>
            <th>Grade Type</th>
            <th>Value</th>
            </tr>
            <?php
            while ($row = mysqli_fetch_assoc($query)) {
              echo "<tr>";
              echo "<td>".$row['Type']."</td>";
              echo "<td>".$row['Value']."</td>";
              echo "</tr>";
            }

            echo "</table>";
          }
             ?>
            </div>

          </div>

        </div>
    <!-- Box End-->



      <!-- Box Content-->
        <div class="col-md-6">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Projects</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">
                <?php
       
      if (isset($_GET['courseCode'])) {
        $code = $_GET['courseCode'];
        $sql = "SELECT title,Description  FROM project WHERE courseCode='$code'";
        $query = mysqli_query($conn,$sql);
        if(!$query) 
          "Error: " . $sql . "<br>" . mysqli_error($conn);

        while ($row = mysqli_fetch_assoc($query)) {
          echo $row['title'];
          echo "Project Description: <br>";
          echo "<a href='".$row['Description']."'>Read Project Document!</a>";
        }

      
      }
     ?>
            </div>

          </div>

        </div>
    <!-- Box End-->








      </div>     


      <?php
             
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
       ?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; CMP-Notifier 2015-2016 <a href="#">CMP Notifier</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>         
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->



          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

  <?php include('../scripts.php');?>    
  </body>
</html>
