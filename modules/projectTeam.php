<?php 
  if(!isset($_SESSION)){
      session_start();
  }

  include_once("../config.php");
  include_once("../function.php");

  if(!isset($_SESSION['Student']))
  { 
      header("Location: /index.php");
  }

  if(isset($_SESSION['Doctor'])){
    $uid = $_SESSION['Doctor']['TeachingID'];
    $UserName = $_SESSION["Doctor"]["UserName"];
    $image = $_SESSION["Doctor"]["Image"];
  }
  else{
    $uid = $_SESSION['Student']['StudentID'];
    $UserName = $_SESSION["Student"]["UserName"];
    $image = $_SESSION["Student"]["Image"];
    $year = $_SESSION["Students"]["Year"];
  }

?>
<!DOCTYPE html>
<html>
  <?php include('../header.php');?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>C</b>MP</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>CMP</b> Notifier</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
            <?php include('../custom-nav_bar.php');?>
        </nav>
      </header>
      <!-- Left side column. conntains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo $image; ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $UserName; ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
          <?php  include ('../menu.php'); ?>        
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- content Wrapper. conntains page content -->
      <div class="content-wrapper">
        <!-- content Header (Page header) -->
        <section class="content-header">
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Article</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content"> 
          <br>
          <div class="row">
            <div class="col-md-12">
              <div class="box box-success box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">
                    <?php
                     $projectID = $_GET['PID'];
                     $getTitle = mysqli_query($conn,"SELECT `title` FROM `project` WHERE `project`.ProjectID = $projectID");
                     if($getTitle){
                          $row = mysqli_fetch_array($getTitle);
                          $title = $row['title'];
                          echo "$title";
                     }else{
                        window.location("/index.php");
                     }
                    ?>
                  </h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                      <i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div>
                <div class="box-body" style="display: block;">
                  <?php
                    $StudentID = $_SESSION['Student']['StudentID'];
                    $query = mysqli_query($conn,"SELECT `student_teams`.`TeamID`, `student_teams`.`TeamLeaderID`, `student_teams`.`Team_Grade`,`user`.`Name` FROM `student_teams`,`user` WHERE `student_teams`.`StudentID` = $StudentID AND `student_teams`.`ProjectID`= $projectID and `user`.UserID = `student_teams`.TeamLeaderID");
                    if($query){
                      if(mysqli_num_rows($query) > 0){
                        echo "<ul>";
                        $row = mysqli_fetch_array($query);
                        echo "<li><h1>The Team Code :". $row['TeamID']."<h1></li>";
                        //View Team Grade
                        if ($row['Team_Grade'] != NULL)
                          echo "<li><h2>Team Grade : ".$row['Team_Grade']."<h2></li>";
                        else
                          echo "<li><h2>Team Grade hasn't been submitted yet</h2></li>";
                        //View Tasks
                        //Add Task to the team members for the team leader
                        if($row['TeamLeaderID']==$StudentID)
                          echo "<li><a href='addtask.php?PID=" . $projectID . "'><h2>Add Tasks to your teammates</h2></a></li>";
                        else //else display the team leader name
                          echo "<li><h2>Your Team Leader is : ". $row['Name'] ."</h2></li>";
                        //View Tasks to the whole team members
                        $tasksRows = mysqli_query($conn,"select content,AnnID from announcements,student_teams where Type = 'task' and ProjectID = $projectID and reciever = StudentID and reciever =$StudentID");
                        if(mysqli_num_rows($tasksRows)>0){
                          echo"<li><h1>Tasks: </h1><ul>";
                          while ($rowTask=mysqli_fetch_array($tasksRows)) {
                            $annID = $rowTask['AnnID'];
                            $content = $rowTask['content'];
                            //determine weather the user saw it or not
                            $seen = 'white';  //default
                            $seenq = mysqli_query($conn,"select * from not_seen_announce where UserID = $StudentID and AnnounceID = $annID");
                            if(mysqli_num_rows($seenq)>0)
                              $seen = 'lightgray';
                            //printing the table
                          echo"<li><h2 style='background-color:".$seen."'>".$content."</h2></li>";
                          }
                          echo "</ul></li>";
                        }else{
                          echo "<li><h2>You don't have any assigned tasks<h2></li>";
                        }
                        //Add Announcement to the team leader only
                        if($row['TeamLeaderID']){
                          if (isset($_POST['Submit'])){
                            $connError = "";
                            $content = "";
                          if(empty($_POST['content']))
                            $connError="Enter the content please";
                          else{
                            $content = $_POST['content'];
                            $TeamID = $row['TeamID'];
                            //add to announcement table
                            $insert = mysqli_query($conn,"insert into announcements (SenderID,IsShared,Type,content,Reciever) values ($StudentID,true,'teamAnnounce','$content',$TeamID)");
                            $AnnounceID = mysqli_insert_id($conn);
                            //add the target to the unseen table
                            $targent = mysqli_query($conn,"select StudentID from student_teams where TeamLeaderID = '$StudentID' and studentID !='$StudentID'");
                            if($targent){
                              $unseeninsert = "insert into not_seen_announce (userID,AnnounceID) values ";
                              while ($row=mysqli_fetch_assoc($targent)){
                                $MemberID = $row['StudentID'];
                                $unseeninsert .= "($MemberID,$AnnounceID),";
                              }
                              $unseeninsert = substr($unseeninsert,0,-1);
                              $unseeninsert .=";";
                              $query = mysqli_query($conn,$unseeninsert);
                            }else{
                              echo "<script>window.alert('error while inserting the announcement');</script>";
                              $delete = mysqli_query($conn,"DELETE FROM `announcements` WHERE AnnID = '$AnnounceID'");
                            }
                          }
                        }
                        if($row['TeamLeaderID']==$StudentID){
                          $form ="<div class='col-md-12'>
                                    <div class='box box-success box-solid'>
                                      <div class='box-header with-border'>
                                        <h3 class='box-title'>Send Announcement to your team members</h3>
                                      </div>
                                          <form method='post'>
                                            <textarea rows= '3' style='font-size:20px;margin: 5px 0' class='form-control with-border' name='content' placeholder='Announcement content' value='";
                          if(isset($content))
                            $form .= $content;
                          $form .= "'></textarea>
                                      <p>";
                          if(isset($connError))
                            $form .= $connError;
                           $form .= "   </p>
                                        <input class='btn btn-block btn-success btn-flat' type='submit' value='Send Announcement' name='Submit'>
                                      </form>
                                    </div>
                                  </div>";
                          echo $form;
                        }
                        //send announcement to the team leader from the team Leader
                        else{
                          if (isset($_POST['Submit1'])){
                            if(empty($_POST['msgcontent'])){
                              $msgconnError="Enter the content please";
                              }
                            else{
                              $msgcontent=$_POST['msgcontent'];
                              $TeamLeaderID = $row['TeamLeaderID'];
                              $insert = mysqli_query($conn,"insert into announcements (SenderID,IsShared,Type,content,reciever) values ($StudentID,false,'message','$msgcontent',$TeamLeaderID)");
                                //add the target to the unseen table
                                $AnnounceID = mysqli_insert_id($conn);
                                if($insert){
                                  $unseeninsert = "insert into not_seen_announce (userID,AnnounceID) values ($TeamLeaderID,$AnnounceID)";
                                  $query = mysqli_query($conn,$unseeninsert);
                                  echo "<script>window.alert('Done');</script>";
                                }
                                else{
                                  echo "<script>window.alert('error while sending the message');</script>";
                                  $delete = "delete from not_seen_announce where AnnounceID=$AnnounceID";
                                }
                            }
                          }

                          $form ="<div class='col-md-12'>
                                    <div class='box box-success box-solid'>
                                      <div class='box-header with-border'>
                                        <h3 class='box-title'>Send Announcement to your team Leader</h3>
                                      </div>
                                          <form method='post'>
                                            <textarea rows= '3' style='font-size:20px;margin: 5px 0' class='form-control with-border' name='msgcontent' placeholder='Message content' value='";
                          if(isset($msgcontent))
                            $form .= $msgcontent;
                          $form .= "'></textarea>
                                      <p>";
                          if(isset($msgconnError))
                            $form .= $msgconnError;
                           $form .= "   </p>
                                        <input class='btn btn-block btn-success btn-flat' type='submit' value='Send Message' name='Submit1'>
                                      </form>
                                    </div>
                                  </div>";
                          echo $form;
                        }
                        }else{
                          echo $row['TeamLeaderID'];
                        }
                      }else
                      echo "<h1>Wrong ID</h1>";
                    }else
                      echo "<h1>Error Has Occured</h1>";
              	 ?>
                </div>
              </div>
            </div>
          </div>     
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; CMP-Notifier 2015-2016 <a href="#">CMP Notifier</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>         
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->



          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.conntrol-sidebar -->
      <div class="conntrol-sidebar-bg"></div>
    </div><!-- ./wrapper -->

  <?php include('../scripts.php');?>    
  </body>
</html>
