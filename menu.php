<li class="header">MAIN NAVIGATION</li>
<?php 
if(!isset($_SESSION))
	session_start();
if(isset($_SESSION['Doctor'])):
include_once("config.php");
include_once("function.php");	

	if(isset($_SESSION['Doctor']))
	{	

		$UserID =  $_SESSION['Doctor']['TeachingID'];		
		$Course = "SELECT * FROM users_courses WHERE UserID = '$UserID'";
		$All_Course = $conn->query($Course);
		if ($All_Course->num_rows > 0):?>

        <li class="treeview">
		<a href="#">
			<i class="fa  fa-bar-chart"></i> <span>Courses</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
			<ul class="treeview-menu">
				
		<?php
    	while($row = $All_Course->fetch_assoc()):?>
        <li>
        <a href="/modules/course.php?courseCode=<?php echo $row['CourseCode'];?>">
        <i class="fa fa-bullhorn"></i>
        <?php 
        $code = $row['CourseCode'];
        $get_name = "SELECT courseName FROM courses WHERE courseCode = '$code'";
        $Course_name = mysqli_fetch_row($conn->query($get_name));		
		echo $Course_name[0];
        ?>	
        </a>
        </li>
    	<?php
    	endwhile;
    	endif;
}	
	
?>
			</ul>
		</li>



<li class="treeview">
<a href="#">
	<i class="fa fa-calendar-minus-o"></i> <span>Events</span> <i class="fa fa-angle-left pull-right"></i>
</a>
	<ul class="treeview-menu">
		<li><a href="/modules/events.php"><i class="fa  fa-eye"></i> View Events   </a></li>
	</ul>
</li>

<li class="treeview">
<a href="#">
	<i class="fa fa-sticky-note-o"></i> <span>Blog</span> <i class="fa fa-angle-left pull-right"></i>
</a>
	<ul class="treeview-menu">
		<li><a href="/modules/Add_blog.php"><i class="fa   fa-plus"></i> Add Article   </a></li>
		<li><a href="/modules/View_article.php"><i class="fa  fa-bullseye"></i> View Blog  </a></li>

	</ul>
</li>

<li class="treeview">
<a href="#">
	<i class="fa  fa-bar-chart"></i> <span>Votes</span> <i class="fa fa-angle-left pull-right"></i>
</a>
	<ul class="treeview-menu">
		<li><a href="/modules/votes.php"><i class="fa fa-bullhorn"></i> View or Make Vote </a></li>
	</ul>
</li>

<li class="treeview">
<a href="#">
	<i class="fa  fa-bar-chart"></i> <span>Announcement</span> <i class="fa fa-angle-left pull-right"></i>
</a>
	<ul class="treeview-menu">
		<li><a href="/modules/add_announce.php"><i class="fa fa-bullhorn"></i> Add Announcement </a></li>
	</ul>
</li>
<?php endif;?>




<?php if(isset($_SESSION['Student'])):?>
<li class="treeview">
<a href="#">
	<i class="fa fa-calendar-minus-o"></i> <span>Events</span> <i class="fa fa-angle-left pull-right"></i>
</a>
	<ul class="treeview-menu">
			<li><a href="/modules/events.php"><i class="fa fa-calendar-plus-o"></i> View Events  </a></li>
	</ul>
</li>

<li class="treeview">
<a href="#">
	<i class="fa fa-sticky-note-o"></i> <span>Blog</span> <i class="fa fa-angle-left pull-right"></i>
</a>
	<ul class="treeview-menu">
		<li><a href="/modules/Add_blog.php"><i class="fa   fa-plus"></i> Add Article   </a></li>
		<li><a href="/modules/View_article.php"><i class="fa  fa-bullseye"></i> View Blog  </a></li>

	</ul>
</li>

<li class="treeview">
		<a href="#">
			<i class="fa  fa-bar-chart"></i> <span>Courses</span> <i class="fa fa-angle-left pull-right"></i>
		</a>
			<ul class="treeview-menu">
				
<?php 

	if(isset($_SESSION['Student']))
	{	

		$UserID =  $_SESSION['Student']['StudentID'];		
		$Course = "SELECT * FROM users_courses WHERE UserID = '$UserID'";
		$All_Course = $conn->query($Course);
		if ($All_Course->num_rows > 0):?>

        
				
		<?php
    	while($row = $All_Course->fetch_assoc()):
    		if($row['CourseCode'] != "Break1" AND $row['CourseCode'] != "Break2" AND $row['CourseCode'] != "Break3" AND $row['CourseCode'] != "Break4"){
    	?>

        <li><a href="/modules/student_course.php?courseCode=<?php echo $row['CourseCode'];?>"><i class="fa fa-bullhorn"></i>
        <?php 
        $code = $row['CourseCode'];
        $get_name = "SELECT courseName FROM courses WHERE courseCode = '$code'";
        $Course_name = mysqli_fetch_row($conn->query($get_name));		
		echo $Course_name[0];

        ?>	
        </a></li>
    	<?php
    }
    	endwhile;
    	endif;
}	
	
?>
			</ul>
		</li>



<li><a href="#">
	<i class="fa fa-user"></i> 
	<span>Schedule</span>
	</a>
</li>


<li class="treeview">
<a href="#">
	<i class="fa  fa-bar-chart"></i> <span>Votes</span> <i class="fa fa-angle-left pull-right"></i>
</a>
	<ul class="treeview-menu">		
		<li><a href="/modules/votes.php"><i class="fa fa-bullhorn"></i><?php if($_SESSION['Student']['IsRep']):?> Make or <?php endif?>view Votes </a></li>
	</ul>
</li>

<?php
	$selectPrj = mysqli_query($conn,"SELECT CourseName,`student_teams`.ProjectID FROM `project`,`courses`,`student_teams` WHERE `student_teams`.StudentID = $UserID and `student_teams`.ProjectID = `project`.`ProjectID` and `project`.CourseCode = `courses`.CourseCode");
	if($selectPrj){
		if(mysqli_num_rows($selectPrj) > 0){
		echo"<li class='treeview'>
				<a href='#'>
				<i class='fa fa-user'></i> 
				<span>Projects Teams</span> <i class='fa fa-angle-left pull-right'></i>
				</a>
				<ul class='treeview-menu'>";
			while($row = mysqli_fetch_array($selectPrj)){
				$PID = $row['ProjectID'];
				$CourseName = $row['CourseName']." Project";
				echo "<li><a href='/modules/projectTeam.php?PID=".$PID."'><i class='fa fa-bullhorn'></i>".$CourseName."</a></li>";
			}
			echo "</ul>
			</li>";
		}else{
			echo"<li class='treeview'>
					<a href='#'>
					<i class='fa fa-user'></i> 
					<span>No Projects Teams yet</span>
					</a>
			</li>";
		}
	}else{
		echo "Error";
	}
?>

<?php
	if(!$_SESSION['Student']['IsRep']):?>
	<li><a href="/modules/contactrep.php">
	<i class="fa fa-user"></i> 
	<span>Contact Representative</span>
	</a>
	</li>
<?php endif?>


<li class="treeview">
<a href="#">
	<i class="fa  fa-bar-chart"></i> <span>Announcement</span> <i class="fa fa-angle-left pull-right"></i>
</a>
	<ul class="treeview-menu">		
		<?php
		if($_SESSION['Student']['IsRep']):?>
			<li><a href="/modules/add_announce.php"><i class="fa fa-bullhorn"></i> Add Announcement </a></li>
		<?php endif?>
		<li><a href="#"><i class="fa fa-user"></i> <span>General Announcement</span></a></li>
	</ul>
</li>


<?php endif;?>
