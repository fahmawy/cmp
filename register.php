<?php 
  session_start();
  include_once("config.php");
  include_once("function.php");

  if(isset($_SESSION['Student']))
  {
    header("Location: student.php");
  }
  elseif(isset($_SESSION['Doctor']))
  {
    header("Location: doctor.php");
  }

  
if(isset($_POST['register']))
  {
    
    if(isset($_POST['user_name']) AND isset($_POST['email']))
    {
      $user_name= $_POST['user_name'];
      $email= $_POST['email'];
    }

    if($_POST['password_1']== $_POST['password_2'])
    {
      $password =  md5($_POST['password_1']);      
    }
    else
    {
      echo '<script>alert("password not like each other")</script>';
    }

    if($_POST['role'] == "Student" )
    {

    $student_code = $_POST['student_code'];
    $student_year = $_POST['year'];
    $student_sec  = $_POST['section'];
    $student_bn   = $_POST['BN'];
    $student_exam = $_POST['exam_bn'];


    $sql = "INSERT INTO user (Name, Password,IsStudent,Email)
    VALUES ('$user_name', '$password' , '1' , '$email')";

    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully";
        
        $LastID = mysqli_insert_id($conn);        
        $insert_student = "INSERT INTO student (StudentID, StudentCode, Year,Section,BN,ExamBN)
        VALUES ('$LastID', '$student_code', '$student_year' , '$student_sec' , '$student_bn','$student_exam')";

        if ($conn->query($insert_student) === TRUE) 
        {
          echo "New record created successfully";
        }
          else 
        {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

        
    } 
    else 
    {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }


    }



    if($_POST['role'] == "Doctor" || $_POST['role'] == "TA"  )
    {
    
    $sql = "INSERT INTO user (Name, Password,IsStudent,Email)
    VALUES ('$user_name', '$password' , '0' , '$email')";

    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully";
        
        $LastID = mysqli_insert_id($conn);        
        $insert_student = "INSERT INTO teaching_staff (TeachingID,IsDoctor)
        VALUES ('$LastID', '1')";

        if ($conn->query($insert_student) === TRUE) 
        {
          echo "New record created successfully";
        }
          else 
        {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

        
    } 
    else 
    {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }


    }

    echo '<meta http-equiv="refresh" content="0; url=index.php" />';
  }


?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CMP Notifier| Registration Page</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/modules/assests/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/modules/assests/bootstrap/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/modules/assests/bootstrap/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/modules/assests/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="/modules/assests/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="background-image: url('/modules/assests/Images/back.jpg'); background-size: cover;" class="hold-transition register-page">

    <div class="register-box">
      <div class="register-logo">
        <a href="register.php"><b>CMP</b>Notifier</a>
      </div>

      <div class="register-box-body">
        <p class="login-box-msg">Register a new membership</p>
        <form  method="post" >
          <div class="form-group has-feedback">
            <input name="user_name" type="text" class="form-control" placeholder="User name">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input name="email" type="email" class="form-control" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input name="password_1" type="password" class="form-control" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input name="password_2" type="password" class="form-control" placeholder="Retype password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <select id="role" name="role"  class="form-control" placeholder="select position">
                <option disabled selected>Select Role</option>
                <option value="Student">Student</option>
                <option value="Doctor">Doctor</option>
                <option value="TA">Teacher Assistant</option>
            </select>
          </div>
          
          <div id="student" class="form-group has-feedback" >
          </div>

          <div class="row">          
            <div class="col-xs">
              <center>
              <button name="register" value="register" type="submit" style="width:90%;"class="btn btn-primary btn-block btn-flat">Register</button>
            </div><!-- /.col -->
          </div>
        </form>

        <div class="social-auth-links text-center">
          
        </div>

        <a href="index.php" class="text-center">I already have a membership</a>
      </div><!-- /.form-box -->
    </div><!-- /.register-box -->

    <!-- jQuery 2.1.4 -->
    <script src="/modules/assests/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="/modules/assests/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="/modules/assests/plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });

    $(document).on('change', '#role', function(e) {
    var user_type = this.options[e.target.selectedIndex].text;    
    if(user_type == "Student")
    {
      var student = $('<div id="other_data"><div class="form-group has-feedback"><input name="student_code" type="text" class="form-control" placeholder="Student Code"><span class="glyphicon glyphicon-education form-control-feedback"></span></div><div class="form-group has-feedback"><select name="year"  class="form-control" placeholder="select position"><option value="1">1st Year</option><option value="2">2nd Year</option><option value="3">3rd Year</option><option value="4">4th Year</option></select></div><div class="form-group has-feedback"><input name="section" type="text" class="form-control" placeholder="section"><span class="glyphicon glyphicon-th form-control-feedback"></span></div><div class="form-group has-feedback"><input name="BN" type="text" class="form-control" placeholder="Bench Number"><span class="glyphicon glyphicon-tag form-control-feedback"></span></div><div class="form-group has-feedback"><input name="exam_bn" type="text" class="form-control" placeholder="Exam Bench Number"><span class="glyphicon glyphicon-warning-sign form-control-feedback"></span></div></div>');
      $('#student').append(student);            
    }

    if(user_type == "Doctor" || user_type == "TA")
    {
      $( "#other_data" ).remove();
    }
    
});

    </script>
  </body>
</html>

