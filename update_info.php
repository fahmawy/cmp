<?php
  include_once("function.php"); 
  include_once("config.php"); 

  if(!isset($_SESSION))
  {
    session_start();
  }

  if(isset($_SESSION['Doctor']))
  {
    $uid = $_SESSION['Doctor']['TeachingID'];
  }
  else
  {
    $uid = $_SESSION['Student']['StudentID'];
  }


  //available updates for all users
  //email
  if(empty($_POST['email'])){
      $getOld = mysqli_query($conn,"select Email from user where UserID = $uid");
      $Email = mysqli_fetch_array($getOld)['Email'];
  }else
      $Email  = $_POST['email'];

  //photo
  if(empty($_POST['photo'])){
      $getOld = mysqli_query($conn,"select Photo from user where UserID = $uid");
      $Photo = mysqli_fetch_array($getOld)['Photo'];
  }else
    $Photo  = $_POST['Photo'];

  //Bio
  if(empty($_POST['bio'])){
      $getOld = mysqli_query($conn,"select Bio from user where UserID = $uid");
      $Bio = mysqli_fetch_array($getOld)['Bio'];
  }else
      $Bio = mysqli_real_escape_string($conn,$_POST['bio']);

  //skills
  if(empty($_POST['skills'])){
      $getOld = mysqli_query($conn,"select skills from user where UserID = $uid");
      $skills = mysqli_fetch_array($getOld)['skills'];
  }else
      $skills  = $_POST['skills'];

  $update = mysqli_query($conn,"Update user SET Email=$Email,Photo=$Photo, Bio=$Bio, skills=$skills");
  
  //Select Courses
  if(!empty($_POST['course'])){
      $delete = mysqli_query($conn,"delete from users_courses where UserID = $uid");
      $selectedCourses = $_POST['course'];
      foreach ($selectedCourses as $course) 
        $insert = mysqli_query($conn,"insert into users_courses (UserID,CourseCode) values($uid,'".$course."')");
      /*$attachedCourses = array();
      $select = mysqli_query($conn,"select CourseCode from users_courses where UserID = $uid");
      while ($row = mysqli_fetch_array($select))
        array_push($attachedCourses, $row['CourseCode']);
      $courses = $_POST['course'];
      foreach ($courses as $course) {
        if(!(in_array($course, $attachedCourses)))
            $insert = mysqli_query($conn,"insert into users_courses (UserID,CourseCode) values($uid,'".$course."')");
      }*/
  }

  if(isset($_SESSION['Doctor'])){
      echo '<meta http-equiv="refresh" content="0; url=doctor.php" />';
  }else{
  //for students only
  if(empty($_POST['BN'])){
      $getOld = mysqli_query($conn,"select BN from student where StudentID = $uid");
      $BN = mysqli_fetch_array($getOld)['BN'];
  }else
      $BN  = $_POST['BN'];

  if(empty($_POST['section'])){
      $getOld = mysqli_query($conn,"select section from student where StudentID = $uid");
      $section = mysqli_fetch_array($getOld)['section'];
  }else
      $section  = $_POST['section'];

  if(empty($_POST['scode'])){
      $getOld = mysqli_query($conn,"select StudentCode from student where StudentID = $uid");
      $SCode = mysqli_fetch_array($getOld)['StudentCode'];
  }else
      $SCode = $_POST ['scode'];
  $update = mysqli_query($conn,"Update student SET BN=$BN,section=$section, StudentCode=$SCode");
  
  echo '<meta http-equiv="refresh" content="0; url=student.php" />';
  }
  
?>