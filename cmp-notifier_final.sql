-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 26, 2015 at 03:30 PM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cmp-notifier`
--

-- --------------------------------------------------------

--
-- Table structure for table `allowed_answers`
--

CREATE TABLE IF NOT EXISTS `allowed_answers` (
  `VotingID` int(10) unsigned NOT NULL COMMENT 'the vote''s auto generated ID',
  `Allowed_Answer` varchar(255) NOT NULL COMMENT 'one of the vote''s allowed answers',
  `VotesNum` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'number of votes to that answer',
  PRIMARY KEY (`VotingID`,`Allowed_Answer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Votes allowed answers and their votes number';

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE IF NOT EXISTS `announcements` (
  `SenderID` int(10) unsigned DEFAULT NULL COMMENT 'the announcement sender ID',
  `AnnID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'the announcement auto generated ID',
  `IsShared` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'if it''s true,this means the announcement has been shared among the recievers',
  `Type` varchar(255) NOT NULL COMMENT 'what this announcement is about(Task\\postponing\\etc.)',
  `Content` text NOT NULL COMMENT 'the content of the announcement itself',
  `Reciever` varchar(10) NOT NULL COMMENT 'the users to which this announcement is sent',
  PRIMARY KEY (`AnnID`),
  KEY `SenderID` (`SenderID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='announcements to different users such as tasks,postpones,etc' AUTO_INCREMENT=17 ;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`SenderID`, `AnnID`, `IsShared`, `Type`, `Content`, `Reciever`) VALUES
(3, 1, 1, 'general', 'You have Exam Tomorrow ', '6'),
(3, 2, 1, 'message', 'we are testing now !!!!!!', '4'),
(3, 14, 0, 'message', 'Ø¨ÙŠØ¨', '5'),
(3, 15, 0, 'message', 'Ø¨ÙŠØ¨Ø³Ø´ÙŠØ³Ø´ÙŠØ´Ø³', '7'),
(3, 16, 0, 'message', 'fahmawy is here', '4');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `AuthorID` int(10) unsigned DEFAULT NULL COMMENT 'the article author ID',
  `ArticleID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'the article auto generated ID',
  `Content` text NOT NULL COMMENT 'the article content',
  `Title` varchar(255) NOT NULL COMMENT 'the article title',
  `IsAccepted` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'if it''s true the article is ok to be published,else it''s rejected',
  PRIMARY KEY (`ArticleID`),
  KEY `AuthorID` (`AuthorID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='the blog articles' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`AuthorID`, `ArticleID`, `Content`, `Title`, `IsAccepted`) VALUES
(6, 1, 'Computer cooling is required to remove the waste heat produced by computer components to keep components within permissible operating temperature limits. Components that are susceptible to temporary malfunction or permanent failure if overheated include integrated circuits such as chipset, graphics card and hard disk drives. Components are often designed to generate as little heat as possible, and computers and operating systems may be designed to reduce power consumption and consequent heating according to workload, but more heat may still be produced than can be removed without attention to cooling. Use of heatsinks cooled by airflow reduces the temperature rise produced by a given amount of heat. Attention to patterns of airflow can prevent the development of hotspots .Computer fans are widely used along with heatsinks to reduce temperature by actively exhausting hot air. There are also more exotic cooling techniques, such as the liquid cooling. All modern day processors are designed to cut out or reduce their voltage (which translates to power usage) and/or clock speed if the internal temperature of the processor exceeds a specified limit. Cooling may be designed to reduce the ambient temperature within the case of a computer e.g. by exhausting hot air, or to cool a single component or small area (spot cooling). Components commonly individually cooled include the CPU and GPU.', 'New Trends in Electronics\r\nCooling System', 1),
(7, 2, 'Reducing the energy consumption of computers is simple. A power managed computer consumes less than half the energy of a computer without power management. Depending on how your computers are used, power management can reduce the annual energy consumption of your computers and by 80%. After lighting, computers and monitors have the highest energy consumption in office environments. Studies have shown that power management of computers and monitors can significantly reduce their energy consumption, saving hundreds or thousands of dollars a year on electricity costs. The energy consumption of computers and monitors is determined by the amount of energy they require to operate and how they are used. While the energy requirements of a device make an important contribution to its overall energy consumption, the key to reducing energy consumption is changing how devices are used. Approximately half of all office computers are left on overnight and on weekends. Evenings and weekends account for 75% of the week, so ensuring computers are turned off at night dramatically reduces their energy consumption. Further savings are made by ensuring computers enter low power mode when they are idle during the day. Power management is a way of ensuring computers and monitors are turned off when not required and in low power mode during idle periods. Manual power management, which relies on education users to turn off pressive results with ongoing education and their computers, can achieve imreinforcement. Alternatively, automatic power management relies on software, or built in energy saving features. Theoretically, automatic power management can achieve 100% power management, with all computers turned off when not required and in low power mode when idle.\r\nThe average computers use 30% of their energy while idle and 40% of their energy outside business hours. Power management reduces the energy consumed by computers and also while they are not in use, this represents a clear opportunity for saving money on energy costs.\r\nThis document reviews studies on computer energy consumption and its balance.\r\nAll of these studies indicate that effective power management significantly reduces the energy consumption of computers.', 'Energy Consumption of Computers and Its Balance', 1),
(4, 3, 'الهجرة من عدمها .. من المسائل الجدلية التي لا ينتهي الكلام فيها. سؤال هل هي فعل صواب ام خطأ لا إجابة نموذجية له. سنسلط الضوء قليلا بهذا المقال علي بعض المغالطات والمصطلحات بإسلوب بسيط ... أو هكذا نأمل !\r\nنبدأ بعرض سريع لعدد من هجرات الجماعات والمجموعات علي مر التاريخ. مثل هذا النوع من الهجرات من الممكن أن تحدث تغييرات ديموغرافية رهيبة وتغير من مجريات الأحداث في البقع الجغرافية المتأثرة.\r\n-	الهجرة التي أذنت ببداية الشعوب,هجرة أبناء سيدنا نوح بعد رسو السفينة وانتشارهم في الأرض. هاجر سام صوب جزيرة العرب وحام صوب مصر وإفريقيا والثالث بإتجاه العراق نهري دجلة والفرات.\r\n-	هجرة الشعوب العبرانية  - شعب سيدنا إبراهيم - من العراق متجهين لساحل المتوسط وفلسطين. تحرك لو لم يتم, لما ولد يعقوب وبنيه بفلسطين,لما دخل العبرانيون مصر مع يوسف, ولما خرجوا مع موسي, ولما حدث اللغط الصهويني اليوم بأحقية اليهود في فلسطين !\r\n-	هجرة الأوروبيون للعالم الجديد وإختفاء نسل الهنود الحمر وظهورأقلية قوية من ذوات البشرة السمراء من أبناء إفريقيا الذين سيقوا لهناك بالملايين للعمل كعبيد.\r\n-	هجرة أعضاء الجماعة اليهودية من حول العالم صوب فلسطين للإستيطان وإقامة دولة إسرائيل.\r\nالأمثلة كثيرة ولا تحصي عن هجرة الجماعات, والهجرة الواحدة نتج عنها أحداث وتقلبات رهيبة. \r\nفلكل هجرة ظروفها ولكل مهاجر أسبابه الخاصة التي تحكِمه في كيفية الهجرة. فالهجرة قد تكون\r\nتحرك داخل حدود البلد نفسه, كهجرة أهل الريف إلي المدن أو تحرك بين حدود الدول والقارات أو قد تكون بسبب  كحركات النزوح نتيجة الحروب او الكوارث.\r\n', 'إن الوطن ليس فندقاً لنتركه حين تسوء الخدمة\r\n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `courseCode` varchar(255) NOT NULL COMMENT 'The Official code of the course',
  `courseName` varchar(255) NOT NULL COMMENT 'the commonly used name of the course',
  `year` tinyint(1) unsigned NOT NULL COMMENT 'for which year the course is taught',
  `Term` tinyint(1) unsigned NOT NULL COMMENT 'if it''s 1, the course is given at the first term ,else if it''s 2 it''s given in the second one,else it''s given at both terms',
  `drive` varchar(255) NOT NULL COMMENT 'coursr Drive link',
  PRIMARY KEY (`courseCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='the Courses taught in the department';

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`courseCode`, `courseName`, `year`, `Term`, `drive`) VALUES
('Break2', 'Activity', 2, 1, ''),
('CIV125', 'Civil Engineering', 1, 1, ''),
('CMP101', 'Logic Design', 1, 1, ''),
('CMP102', 'Data Structures', 1, 1, ''),
('CMP201', 'Microprocesor', 2, 1, 'https://drive.google.com/drive/u/3/folders/0B2_Pc4KsrpHyc0FjT3dfY21FRWc'),
('CMP202', 'Database', 2, 1, 'https://drive.google.com/drive/u/3/folders/0B2_Pc4KsrpHyT2dLWVEwdWtMaHM'),
('CMP205', 'Graphics', 2, 1, 'https://drive.google.com/drive/u/3/folders/0B2_Pc4KsrpHyS0YyMzNUVlZ4dlU'),
('CMP301', 'Computer Architecture', 3, 1, ''),
('CMP302', 'Algorithms', 3, 1, ''),
('CMP303', 'Operating Systems', 3, 1, ''),
('CMP401', 'Advanced Database', 4, 1, ''),
('CMP402', 'Artificial intelligence  ', 4, 1, ''),
('CMP405', 'Network', 4, 1, ''),
('CMP407', 'Modeling', 4, 1, ''),
('CMP425', 'Consultation', 4, 1, ''),
('CMP461', 'Testing', 4, 1, ''),
('ELC125A', 'Electric Circuits', 1, 1, ''),
('ELC225A', 'Electronic Cicrcuits', 2, 1, 'https://drive.google.com/drive/u/3/folders/0B2_Pc4KsrpHycVdWUk53b0pCRm8'),
('ELC325', 'Analog Communications', 3, 1, ''),
('EPM225', 'Power and machines', 2, 1, 'https://drive.google.com/drive/u/3/folders/0B2_Pc4KsrpHydXdDMnlTc25rR3M'),
('GEN125', 'Presentation Skills', 1, 1, ''),
('GEN225', 'Project Management', 2, 1, 'https://drive.google.com/drive/u/3/folders/0B2_Pc4KsrpHyQWtUZHcyc2RHZ0k'),
('GEN325', 'Marketing', 3, 1, ''),
('Lab', 'Physics Lab', 1, 1, ''),
('Lab-Sec1', 'Lab', 1, 1, ''),
('Lab-Sec2', 'Lab', 1, 1, ''),
('MCH125', 'Robotics and Thermodynamics', 1, 2, ''),
('MTH125', 'Math(2)', 1, 1, ''),
('MTH225A', 'Math(3)', 2, 1, 'https://drive.google.com/drive/u/3/folders/0B2_Pc4KsrpHyZlJKSTNhRENWZFE'),
('MTH325', 'Numerical Analysis', 3, 1, ''),
('Physics125A', 'Physics', 1, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `course_references`
--

CREATE TABLE IF NOT EXISTS `course_references` (
  `courseCode` varchar(255) NOT NULL COMMENT 'The Official code of the course for which this reference is used',
  `ReferenceISBN` varchar(17) NOT NULL COMMENT 'The reference official ISBN',
  `ReferenceName` varchar(255) DEFAULT NULL COMMENT 'the reference title',
  PRIMARY KEY (`courseCode`,`ReferenceISBN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The courses'' references';

--
-- Dumping data for table `course_references`
--

INSERT INTO `course_references` (`courseCode`, `ReferenceISBN`, `ReferenceName`) VALUES
('CMP201', '', ''),
('CMP201', '123456', 'Communication');

-- --------------------------------------------------------

--
-- Table structure for table `course_schedule`
--

CREATE TABLE IF NOT EXISTS `course_schedule` (
  `courseCode` varchar(10) NOT NULL COMMENT 'The Official code of the course',
  `Day` varchar(10) NOT NULL COMMENT 'the day in which the course is taught',
  `LectureNum` tinyint(1) unsigned NOT NULL COMMENT 'at which lecture it''s given',
  `IsSection` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'if it''s true,that interval is a section,else it''s a lecture',
  `Alternating` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'A number that indicates how many lectures\\sections alternating with that one at the same time in the schedule',
  PRIMARY KEY (`courseCode`,`Day`,`LectureNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='the full description of the courses'' schedule';

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `EventID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'the event''s auto generated ID',
  `CreatorID` int(10) unsigned DEFAULT NULL COMMENT 'the event creator ID',
  `Description` varchar(255) NOT NULL COMMENT 'the event''s description',
  `Place` varchar(255) NOT NULL COMMENT 'where the event is hold',
  `Time` datetime NOT NULL COMMENT 'when the event is hold',
  `IsConfirmed` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'if it''s true, the event is ok to be hold',
  `Title` varchar(255) NOT NULL COMMENT 'Events Header',
  `Target` int(1) unsigned NOT NULL COMMENT 'The target of the event',
  PRIMARY KEY (`EventID`),
  KEY `CreatorID` (`CreatorID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE IF NOT EXISTS `grades` (
  `StudentID` int(10) unsigned NOT NULL COMMENT 'the student for which this grade is assgined',
  `CourseCode` varchar(255) NOT NULL COMMENT 'the course in which this grade is assgined',
  `Type` varchar(10) NOT NULL COMMENT 'Quiz\\ midterm\\...',
  `Value` float unsigned NOT NULL COMMENT 'the grade of that type for that student in that course',
  PRIMARY KEY (`StudentID`,`CourseCode`,`Type`),
  KEY `Course_Grades` (`CourseCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The grades of the students in each course';

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`StudentID`, `CourseCode`, `Type`, `Value`) VALUES
(6, 'CMP201', 'Labs', 9.8),
(6, 'CMP201', 'Midterm', 7.6),
(6, 'CMP201', 'Project', 19),
(6, 'CMP201', 'Sheets', 9.6),
(7, 'CMP201', 'Labs', 9.8),
(7, 'CMP201', 'Midterm', 10.5),
(7, 'CMP201', 'Project', 19),
(7, 'CMP201', 'Sheets', 9);

-- --------------------------------------------------------

--
-- Table structure for table `not_ans_votes`
--

CREATE TABLE IF NOT EXISTS `not_ans_votes` (
  `UserID` int(10) unsigned NOT NULL COMMENT 'the user''s ID',
  `VotingID` int(10) unsigned NOT NULL COMMENT 'the vote''s ID',
  PRIMARY KEY (`UserID`,`VotingID`),
  KEY `Vote_Was_Answered` (`VotingID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The votes that the user hasn''t answered yet';

-- --------------------------------------------------------

--
-- Table structure for table `not_seen_announce`
--

CREATE TABLE IF NOT EXISTS `not_seen_announce` (
  `UserID` int(10) unsigned NOT NULL COMMENT 'the user''s ID',
  `AnnounceID` int(10) unsigned NOT NULL COMMENT 'the announcement ID',
  PRIMARY KEY (`UserID`,`AnnounceID`),
  KEY `Ann_Is_Seen` (`AnnounceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='the announcements that the user hasn''t seen yet';

--
-- Dumping data for table `not_seen_announce`
--

INSERT INTO `not_seen_announce` (`UserID`, `AnnounceID`) VALUES
(3, 2),
(3, 14),
(3, 15),
(3, 16);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `courseCode` varchar(10) DEFAULT NULL COMMENT 'The Official code of the course this project is made',
  `ProjectID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'the number of existing projects in this course +1',
  `Description` varchar(255) NOT NULL COMMENT 'The project’s official description link.',
  `Year` tinyint(1) unsigned NOT NULL COMMENT 'for which educational year the project is assigned',
  `title` varchar(255) NOT NULL COMMENT 'the project title',
  PRIMARY KEY (`ProjectID`),
  KEY `courseCode` (`courseCode`),
  KEY `courseCode_2` (`courseCode`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='the courses'' projects full description' AUTO_INCREMENT=8 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`courseCode`, `ProjectID`, `Description`, `Year`, `title`) VALUES
('CMP201', 7, '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `StudentID` int(10) unsigned NOT NULL COMMENT 'the student''s ID',
  `StudentCode` int(10) NOT NULL COMMENT 'the student official code',
  `Year` tinyint(1) unsigned NOT NULL COMMENT 'in which year the student is',
  `Section` tinyint(2) unsigned NOT NULL COMMENT 'the student''s section number',
  `BN` tinyint(2) unsigned NOT NULL COMMENT 'the student''s BN in the section',
  `IsRep` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'if the student is a representative it''s true, else it''s false',
  `ExamBN` int(10) unsigned DEFAULT NULL COMMENT 'the student''s BN in the final exams',
  PRIMARY KEY (`StudentID`),
  UNIQUE KEY `StudentCode` (`StudentCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`StudentID`, `StudentCode`, `Year`, `Section`, `BN`, `IsRep`, `ExamBN`) VALUES
(4, 9091542, 2, 2, 1, 0, NULL),
(5, 9130390, 2, 1, 9, 0, NULL),
(6, 9131079, 2, 2, 20, 1, NULL),
(7, 9130882, 2, 1, 9, 0, NULL),
(32, 9131080, 2, 2, 25, 0, NULL),
(33, 9130690, 2, 1, 26, 0, NULL),
(34, 9130703, 2, 1, 30, 0, NULL),
(35, 98798, 3, 2, 27, 0, NULL),
(36, 87987, 4, 2, 32, 0, NULL),
(37, 85521, 1, 1, 16, 0, NULL),
(38, 784512, 2, 1, 15, 0, NULL),
(39, 78955, 2, 1, 31, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `student_teams`
--

CREATE TABLE IF NOT EXISTS `student_teams` (
  `StudentID` int(10) unsigned NOT NULL COMMENT 'the student given ID',
  `ProjectID` int(10) unsigned NOT NULL COMMENT 'The project’s given ID',
  `TeamID` int(10) unsigned NOT NULL COMMENT 'the team in which this student is',
  `Team_Grade` int(10) unsigned DEFAULT NULL COMMENT 'the team''s whole grade',
  `TeamLeaderID` int(10) unsigned DEFAULT NULL COMMENT 'the team leader of that team',
  PRIMARY KEY (`StudentID`,`ProjectID`),
  KEY `Project_Teams` (`ProjectID`),
  KEY `TeamLeaderID` (`TeamLeaderID`),
  KEY `TeamLeaderID_2` (`TeamLeaderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='the student''s team in each project';

-- --------------------------------------------------------

--
-- Table structure for table `submitted_answers`
--

CREATE TABLE IF NOT EXISTS `submitted_answers` (
  `VotingID` int(10) unsigned NOT NULL COMMENT 'the vote given id',
  `UserID` int(10) unsigned NOT NULL COMMENT 'the voting user',
  `SubAns` varchar(255) NOT NULL COMMENT 'the submitted answer from the user',
  PRIMARY KEY (`VotingID`,`UserID`),
  KEY `SubAns` (`SubAns`(191)),
  KEY `User_Vote` (`UserID`),
  KEY `Vote_Answer` (`VotingID`,`SubAns`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `teaching_staff`
--

CREATE TABLE IF NOT EXISTS `teaching_staff` (
  `TeachingID` int(10) unsigned NOT NULL COMMENT 'the teaching staff member''s ID',
  `IsDoctor` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'if it''s true, this member is a doctor,else it''s a teaching assistant',
  PRIMARY KEY (`TeachingID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='the teaching staff members table';

--
-- Dumping data for table `teaching_staff`
--

INSERT INTO `teaching_staff` (`TeachingID`, `IsDoctor`) VALUES
(1, 0),
(2, 1),
(3, 1),
(8, 0),
(9, 0),
(10, 1),
(11, 0),
(12, 0),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 0),
(18, 0),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 0),
(28, 1),
(29, 1),
(30, 0),
(31, 1);

-- --------------------------------------------------------

--
-- Table structure for table `teaching_staff_oh`
--

CREATE TABLE IF NOT EXISTS `teaching_staff_oh` (
  `TeachingStaffID` int(10) unsigned NOT NULL COMMENT 'the teaching staff member given ID',
  `Day` varchar(10) NOT NULL COMMENT 'the day in which the office hours are',
  `FromTime` varchar(5) NOT NULL COMMENT 'on the form 00:00 AM/PM',
  `ToTime` varchar(5) NOT NULL COMMENT 'on the form 00:00',
  PRIMARY KEY (`TeachingStaffID`,`Day`,`FromTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='the office hours for each teaching staff member';

--
-- Dumping data for table `teaching_staff_oh`
--

INSERT INTO `teaching_staff_oh` (`TeachingStaffID`, `Day`, `FromTime`, `ToTime`) VALUES
(1, 'Monday', '12:15', '3:30 '),
(1, 'Tuesday', '12:15', '1:45 '),
(2, 'Sunday', '12:00', '14:00'),
(8, 'Monday', '13:00', '14:00'),
(8, 'Sunday', '2:00 ', '3:30 '),
(16, 'Monday', '1:00 ', '2:00 '),
(17, 'Monday', '12:15', '3:30 '),
(17, 'Sunday', '2:00 ', '3:30 '),
(19, 'Monday', '11:00', '1:00 '),
(19, 'Tuesday', '8:00 ', '1:00 '),
(21, 'Sunday', '10:00', '12:00'),
(26, 'Wednesday', '9:00 ', '12:00'),
(30, 'Sunday', '8:30 ', '10:00'),
(30, 'Wednesday', '8:30 ', '10:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `UserID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'the user''s ID',
  `Name` varchar(255) NOT NULL COMMENT 'the user''s name',
  `Password` varchar(32) NOT NULL COMMENT 'the user''s password',
  `IsStudent` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'if it''s true, it''s a student...else it''s a teaching staff member',
  `Email` varchar(255) NOT NULL COMMENT 'the user''s email',
  `Photo` text,
  `Bio` text NOT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `skills` text,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='the whole users table (Students and teaching staff members)' AUTO_INCREMENT=42 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`UserID`, `Name`, `Password`, `IsStudent`, `Email`, `Photo`, `Bio`, `Title`, `skills`) VALUES
(1, 'Eman Hossam', '123456', 0, 'eman.hosam@gmail.cm', NULL, '', NULL, NULL),
(2, 'Hatem ElBoghdady', '123456', 0, 'hatim@gmail.com', NULL, '', NULL, NULL),
(3, 'Magda Fayek', '1acac49261343b8aba73ac181aac8efb', 0, 'me@mail.com', 'http://www.joomlack.fr/images/demos/demo2/on-top-of-earth.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n', 'Doctor', NULL),
(4, 'Fahmy Sayed Fahmy', '1acac49261343b8aba73ac181aac8efb', 1, 'me@fahmawy.com', 'http://www.joomlack.fr/images/demos/demo2/on-top-of-earth.jpg', 'lablalablalablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla lablalabla  ', 'Student', 'html,css,php,js,windowsphone'),
(5, 'Bassel Hossam', '1acac49261343b8aba73ac181aac8efb', 1, 'basel@cmpnotifier.net', NULL, '', NULL, NULL),
(6, 'Menna Mustafa', '1acac49261343b8aba73ac181aac8efb', 1, 'mennamustafa.f@gmail.com', 'http://assets.nydailynews.com/polopoly_fs/1.2470151!/img/httpImage/image.jpg_gen/derivatives/landscape_300/image.jpg', 'Computer Engineering student ', NULL, 'HTMl,CSS,JavaScript,JQuery,Database,PHP,Photoshop'),
(7, 'Mohamed Atef', '1acac49261343b8aba73ac181aac8efb', 1, 'mohamedatefmohamed95@gmail.com', NULL, 'Computer Engineering Student', NULL, 'HTMl,CSS,Javascript,PHP,Database,Problem solving'),
(8, 'Sarah Rashad', '123456', 0, 'sarah_rashad_taha@yahoo.com\r\n', NULL, 'Teaching Assistant in Computer Department', NULL, 'Microprocessor'),
(9, 'Lydia Wahid', '123456', 0, 'eng.lydiawahid@hotmail.com', NULL, 'Teacher Assistant in Computer Department', NULL, 'Computer Graphics, Microprocssor'),
(10, 'Nessreen Elabiad', '123456', 0, 'nelabiad@mcit.gov.eg', NULL, 'HR at ITI', NULL, 'Presentation Skills'),
(11, 'Mohamed Ismail', '123456', 0, 'Mohismail@gmail.com', NULL, 'Teaching Assistant at Computer department', NULL, 'Data Structures, Database'),
(12, 'Mohamed Assem', '123456', 0, 'mohamedassemqa@outlook.com', NULL, 'Teaching Assistant at Computer deartment', NULL, 'Logic Design'),
(13, 'Sameh Bedair', '123456', 0, 'samehbedair@gmail.com', NULL, '', NULL, 'Project Management'),
(14, 'Mayada Hadhoud', '123456', 0, 'mayadahadhoud@gmail.com', NULL, '', NULL, 'Database'),
(15, 'Mohamed Rehan', '123456', 0, 'mohamed.rehan@gmail.com', NULL, '', NULL, 'Computer Graphics'),
(16, 'Ihab Talkhan', '123456', 0, 'italkhan@cu.edu.eg', NULL, 'Computer Department''s head', NULL, 'Logic design'),
(17, 'Asmaa Rabie', '123456', 0, 'asmaa.rabie.abdulaziz@gmail.com', NULL, 'Teaching Assistants at Computer Department', NULL, 'Database'),
(18, 'Dina Tantawy', '123456', 0, 'd.tantawy@gmail.com', NULL, 'Teaching Assistant', NULL, NULL),
(19, 'Neveen Darwish', '123456', 0, 'ndarwish@ieee.org', NULL, '', NULL, NULL),
(20, 'Ahmed Darwish', '123456', 0, 'darwish@ieee.org', NULL, '', NULL, NULL),
(21, 'Samir Shaheen', '123456', 0, 'sshaheen@ieee.org', NULL, '', NULL, NULL),
(22, 'Hwayda Farouk', '123456', 0, 'hismaeel@aucegypt.edu', NULL, '', NULL, NULL),
(23, 'Ali Fahmy', '123456', 0, 'afahmy@eng.cu.edu.eg', NULL, '', NULL, NULL),
(24, 'Ahmed Mourgan', '123456', 0, 'aahmorgan@yahoo.com', NULL, '', NULL, NULL),
(25, 'Mona Farouk', '123456', 0, 'Mona_farouk_ahmed@yahoo.com', NULL, '', NULL, NULL),
(26, 'Amr Wassal', '123456', 0, 'a_wassal@yahoo.com', NULL, '', NULL, NULL),
(27, 'Marwa Hassan', '123456', 0, 'eng.mmahmoud@yahoo.com', NULL, '', NULL, NULL),
(28, 'Tarek Hindam', '123456', 0, 'tarekhindam@eng.cu.edu.eg', NULL, '', NULL, NULL),
(29, 'Ahmed Sobih', '123456', 0, 's_ahmed_adel@yahoo.com', NULL, '', NULL, NULL),
(30, 'Osama Selim', '123456', 0, 'osama.selim@live.com', NULL, 'Teaching Assistant at Mechanical power department', NULL, NULL),
(31, 'Samy S. Soliman', '123456', 1, 'samy.soliman@cu.edu.eg', NULL, '', NULL, 'Electronics,Communications '),
(32, 'Mayar Ahmed', '654321', 1, 'mayar.ahefny@gmail.com', NULL, '', NULL, NULL),
(33, 'Omar Osama', '654321', 1, 'omarosamafahmy@gmail.com', NULL, '', NULL, NULL),
(34, 'Omar Abdulrahman', '654321', 1, 'o.3ammor@gmail.com', NULL, '', NULL, NULL),
(35, 'Hanaa Mohamed', '654321', 1, 'hanaamohamed2055@outlook.com', NULL, '', NULL, NULL),
(36, 'Yasser Farouk', 'c33367701511b4f6020ec61ded352059', 1, 'y.farouk@gmail.com', NULL, '', NULL, NULL),
(37, 'Hassan Saied Hassan', '654321', 1, 'hsaied117@yahoo.com', NULL, '', NULL, NULL),
(38, 'Rania Alaa', '654321', 1, 'rania.alaaa@yahoo.com', NULL, '', NULL, NULL),
(39, 'Omar Sameh Tantawy', '654321', 1, 'omartantawy94@gmail.com', NULL, '', NULL, NULL),
(40, 'Fouad ashraf fouad', '654321', 1, 'eng.fouad.ashraf@gmail.com', NULL, 'i''m computer engineer who aim to develop something to help people.', NULL, 'playing football,writing poems and director of silhouette in theaters.  '),
(41, 'Esraa Mostafa', '654321', 1, 'esraa_mostafa94@yahoo.com', NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_courses`
--

CREATE TABLE IF NOT EXISTS `users_courses` (
  `UserID` int(10) unsigned NOT NULL COMMENT 'the user who teach\\learn this course',
  `CourseCode` varchar(255) NOT NULL COMMENT 'the course itself',
  PRIMARY KEY (`UserID`,`CourseCode`),
  KEY `Course_Student` (`CourseCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='the courses that is related to the user(teaching\\learning)';

--
-- Dumping data for table `users_courses`
--

INSERT INTO `users_courses` (`UserID`, `CourseCode`) VALUES
(6, 'Break2'),
(12, 'CMP101'),
(16, 'CMP101'),
(37, 'CMP101'),
(1, 'CMP102'),
(3, 'CMP102'),
(11, 'CMP102'),
(37, 'CMP102'),
(2, 'CMP201'),
(6, 'CMP201'),
(7, 'CMP201'),
(8, 'CMP201'),
(9, 'CMP201'),
(6, 'CMP202'),
(7, 'CMP202'),
(11, 'CMP202'),
(14, 'CMP202'),
(17, 'CMP202'),
(6, 'CMP205'),
(7, 'CMP205'),
(9, 'CMP205'),
(15, 'CMP205'),
(22, 'CMP301'),
(35, 'CMP301'),
(14, 'CMP302'),
(35, 'CMP302'),
(25, 'CMP303'),
(35, 'CMP303'),
(25, 'CMP401'),
(36, 'CMP401'),
(19, 'CMP402'),
(36, 'CMP402'),
(36, 'CMP405'),
(36, 'CMP407'),
(21, 'CMP425'),
(36, 'CMP425'),
(29, 'CMP461'),
(36, 'CMP461'),
(37, 'ELC125A'),
(6, 'ELC225A'),
(7, 'ELC225A'),
(31, 'ELC225A'),
(31, 'ELC325'),
(35, 'ELC325'),
(6, 'EPM225'),
(10, 'GEN125'),
(6, 'GEN225'),
(7, 'GEN225'),
(13, 'GEN225'),
(35, 'GEN325'),
(37, 'Lab'),
(37, 'Lab-Sec1'),
(30, 'MCH125'),
(37, 'MCH125'),
(37, 'MTH125'),
(6, 'MTH225A'),
(7, 'MTH225A'),
(35, 'MTH325'),
(37, 'Physics125A');

-- --------------------------------------------------------

--
-- Table structure for table `users_phones`
--

CREATE TABLE IF NOT EXISTS `users_phones` (
  `UserID` int(10) unsigned NOT NULL COMMENT 'the user who has this phone number',
  `PhoneNumber` varchar(11) NOT NULL COMMENT 'the phone number itself',
  PRIMARY KEY (`UserID`,`PhoneNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='the phone numbers of the users';

--
-- Dumping data for table `users_phones`
--

INSERT INTO `users_phones` (`UserID`, `PhoneNumber`) VALUES
(1, '01001688379'),
(4, '01146114611'),
(5, '01148223074'),
(6, '01154553444'),
(7, '01275709908'),
(9, '01221034561'),
(10, '01226161144'),
(12, '01007846437'),
(13, '01223149958'),
(15, '01225550461'),
(30, '01285262268'),
(31, '01280914108'),
(33, '01277699097'),
(34, '01067607444');

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

CREATE TABLE IF NOT EXISTS `votes` (
  `VotingID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'the vote''s auto generated ID',
  `MakerID` int(10) unsigned DEFAULT NULL COMMENT 'the maker''s given ID',
  `Reciever` varchar(255) NOT NULL COMMENT 'the vote''s recievers',
  `Question` varchar(255) NOT NULL COMMENT 'the asked question',
  `Expire_Date` datetime NOT NULL COMMENT 'the expire date to submit an answer',
  PRIMARY KEY (`VotingID`),
  KEY `MakerID` (`MakerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `allowed_answers`
--
ALTER TABLE `allowed_answers`
  ADD CONSTRAINT `Vote_Allow_Answers` FOREIGN KEY (`VotingID`) REFERENCES `votes` (`VotingID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `announcements`
--
ALTER TABLE `announcements`
  ADD CONSTRAINT `User_Announce` FOREIGN KEY (`SenderID`) REFERENCES `user` (`UserID`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `blog`
--
ALTER TABLE `blog`
  ADD CONSTRAINT `User_Write` FOREIGN KEY (`AuthorID`) REFERENCES `user` (`UserID`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `course_references`
--
ALTER TABLE `course_references`
  ADD CONSTRAINT `Course_Ref` FOREIGN KEY (`courseCode`) REFERENCES `courses` (`courseCode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `course_schedule`
--
ALTER TABLE `course_schedule`
  ADD CONSTRAINT `Course_Schedule` FOREIGN KEY (`courseCode`) REFERENCES `courses` (`courseCode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `User_Add_Event` FOREIGN KEY (`CreatorID`) REFERENCES `user` (`UserID`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `grades`
--
ALTER TABLE `grades`
  ADD CONSTRAINT `Course_Grades` FOREIGN KEY (`CourseCode`) REFERENCES `courses` (`courseCode`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Student_Grade` FOREIGN KEY (`StudentID`) REFERENCES `student` (`StudentID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `not_ans_votes`
--
ALTER TABLE `not_ans_votes`
  ADD CONSTRAINT `user_must_Answer_Vote` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Vote_mustBe_Answered` FOREIGN KEY (`VotingID`) REFERENCES `votes` (`VotingID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `not_seen_announce`
--
ALTER TABLE `not_seen_announce`
  ADD CONSTRAINT `Ann_Is_Seen` FOREIGN KEY (`AnnounceID`) REFERENCES `announcements` (`AnnID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `User_See_Ann` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `CourseProject` FOREIGN KEY (`courseCode`) REFERENCES `courses` (`courseCode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `Student_Is_User` FOREIGN KEY (`StudentID`) REFERENCES `user` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `student_teams`
--
ALTER TABLE `student_teams`
  ADD CONSTRAINT `Project_Teams` FOREIGN KEY (`ProjectID`) REFERENCES `project` (`ProjectID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Students_Leaders` FOREIGN KEY (`TeamLeaderID`) REFERENCES `student` (`StudentID`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Student_Team` FOREIGN KEY (`StudentID`) REFERENCES `student` (`StudentID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `submitted_answers`
--
ALTER TABLE `submitted_answers`
  ADD CONSTRAINT `User_Vote` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Vote_Answer` FOREIGN KEY (`VotingID`, `SubAns`) REFERENCES `allowed_answers` (`VotingID`, `Allowed_Answer`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teaching_staff`
--
ALTER TABLE `teaching_staff`
  ADD CONSTRAINT `Teaching_Is_User` FOREIGN KEY (`TeachingID`) REFERENCES `user` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teaching_staff_oh`
--
ALTER TABLE `teaching_staff_oh`
  ADD CONSTRAINT `Teaching_OH` FOREIGN KEY (`TeachingStaffID`) REFERENCES `teaching_staff` (`TeachingID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_courses`
--
ALTER TABLE `users_courses`
  ADD CONSTRAINT `Course_Student` FOREIGN KEY (`CourseCode`) REFERENCES `courses` (`courseCode`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `User_Course` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_phones`
--
ALTER TABLE `users_phones`
  ADD CONSTRAINT `User_Phone` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `votes`
--
ALTER TABLE `votes`
  ADD CONSTRAINT `User_Make_Vote` FOREIGN KEY (`MakerID`) REFERENCES `user` (`UserID`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
