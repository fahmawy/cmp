<?php 
  include_once("config.php");
  include_once("function.php");
  if(!isset($_SESSION)){
      session_start();
  }
  
  if(!isset($_SESSION['Student']))
  { 
      header("Location: student.php");
  }

?>
<!DOCTYPE html>
<html>
  <?php include('header.php');?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>C</b>MP</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>CMP</b> Notifier</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <?php include('custom-nav_bar.php');?>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo $_SESSION["Student"]["Image"];?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION["Student"]["UserName"];?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
          <?php  include ('menu.php'); ?>                     
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
      <section class="content">      
      <?php
              // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        else
        {
          // echo "Connected successfully"; 
           //pr($_SESSION["Student"]);
          
           $ID = $_SESSION['Student']['StudentID'];


           $User_Photo = "SELECT Photo FROM user WHERE UserID = $ID";
           $Image = mysqli_fetch_row($conn->query($User_Photo));


           $Title = "SELECT Title FROM user WHERE UserID = $ID";
           $User_Title = mysqli_fetch_row($conn->query($Title));

           $get_bio = "SELECT Bio FROM user WHERE UserID = $ID";
           $Bio = mysqli_fetch_row($conn->query($get_bio));

           $get_skills = "SELECT skills FROM user WHERE UserID = $ID";
           $Skills = mysqli_fetch_row($conn->query($get_skills));
           
        } 

       ?>

<div class="row">
        <div class="col-md-4">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img style="width:128px; height:128px;" class="profile-user-img img-responsive img-circle" src="<?php echo $_SESSION['Student']['Image'];?>" alt="User profile picture">

              <h3 class="profile-username text-center">
              <?php
                     echo $_SESSION['Student']['UserName'];

              ?>
              </h3>

              <p class="text-muted text-center"><?php echo $User_Title[0];?></p>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Bio</strong>

              <p class="text-muted">
                <?php echo $Bio[0];?>
              </p>

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>


              <p>
                <?php
                $All_Skills = explode(',',$Skills[0]);                
                $i = 0;
                 while($i < count($All_Skills)):?>
                <span class="label label-danger"><?php echo $All_Skills[$i]; $i++;?></span>
                <span class="label label-success"><?php echo $All_Skills[$i]; $i++;?></span>
                <span class="label label-info"><?php echo $All_Skills[$i]; $i++;?></span>
                <span class="label label-warning"><?php echo $All_Skills[$i]; $i++;?></span>
                <span class="label label-primary"><?php echo $All_Skills[$i]; $i++;?></span>
              <?php endwhile;?>
              </p>


            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-8">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Activity</a></li>              
              <li class=""><a href="#settings" data-toggle="tab" aria-expanded="false">Settings</a></li>
              <li class=""><a href="#changepass" data-toggle="tab" aria-expanded="false">Change Password</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="activity">
                <!-- Post -->
                <div class="post">
                  <div class="user-block">
                    <img class="img-circle img-bordered-sm" src="<?php echo $_SESSION["Student"]["Image"];?>" alt="user image">
                        <span class="username">
                          <a href="#">Jonathan Burke Jr.</a>
                          <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                        </span>
                    <span class="description">Shared publicly - 7:30 PM today</span>
                  </div>
                  <!-- /.user-block -->
                  <p>
                    Lorem ipsum represents a long-held tradition for designers,
                    typographers and the like. Some people hate it and argue for
                    its demise, but others ignore the hate as they create awesome
                    tools to help create filler text for everyone from bacon lovers
                    to Charlie Sheen fans.
                  </p>
                  <ul class="list-inline">
                    <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li>
                    <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a>
                    </li>
                    <li class="pull-right">
                      <a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Comments
                        (5)</a></li>
                  </ul>

                  <input class="form-control input-sm" type="text" placeholder="Type a comment">
                </div>
                <!-- /.post -->

                
              </div>
              <!-- /.tab-pane -->
              


              <div class="tab-pane" id="settings">

              <?php 
              $UserID = $_SESSION['Student']['StudentID'];
              $Student_Data = "SELECT * FROM user WHERE UserID = '$UserID'";
              $data = mysqli_fetch_row($conn->query($Student_Data));    
              ?>
                <form  class="form-horizontal" method="POST" action="update_info.php">
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-10">
                      <input name="usr_name" type="text" disabled class="form-control" id="inputName" value="<?php echo $data[1];?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-10">
                      <input name="email" type="email" class="form-control" id="inputEmail" value="<?php echo $data[4]; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputExperience" class="col-sm-2 control-label">Biography</label>

                    <div class="col-sm-10">
                      <textarea name="bio" class="form-control" id="inputExperience"><?php echo $data[6];?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Skills</label>

                    <div class="col-sm-10">
                      <input name="skills" type="text" class="form-control" id="inputSkills" value="<?php echo $data[8]; ?>">
                    </div>
                  </div>                    
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                       <label>
                          Note: Enter Your Skills Seperated By , comma
                        </label>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Photo</label>

                    <div class="col-sm-10">
                      <input name="photo" type="text" class="form-control" id="inputSkills" value="<?php echo $data[5]; ?>">
                    </div>
                  </div>  


                  	<!-- Edit BN -->
                  	<?php
                  		$sql = "SELECT BN FROM student WHERE StudentID = $data[0]";
                  		$query= mysqli_query($conn,$sql);
                  		$BN="";
                  		while($row = mysqli_fetch_assoc($query))
                  			$BN = $row['BN'];
                  	?>
                  	<div class="form-group">
	                    <label for="inputBN" class="col-sm-2 control-label">BN</label>
	                    <div class="col-sm-10">
	                      <input name="BN" type="number" class="form-control" id="inputBN" value="<?php echo $BN; ?>">
	                    </div>
                  	</div>

                  	<!-- Edit Section -->
                  	<?php
                  		$sql = "SELECT Section FROM student WHERE StudentID = $data[0]";
                  		$query= mysqli_query($conn,$sql);
                  		$Sec="";
                  		while($row = mysqli_fetch_assoc($query))
                  			$Sec = $row['Section'];
                  	?>
                  	<div class="form-group">
	                    <label for="inputSection" class="col-sm-2 control-label">Section</label>
	                    <div class="col-sm-10">
	                      <input name="section" type="number" class="form-control" id="inputSection" value="<?php echo $Sec; ?>">
	                    </div>
                  	</div>

                  	<!-- Edit Student Code -->
                  	<?php
                  		$sql = "SELECT StudentCode FROM student WHERE StudentID = $data[0]";
                  		$query= mysqli_query($conn,$sql);
                  		$SCode="";
                  		while($row = mysqli_fetch_assoc($query))
                  			$SCode = $row['StudentCode'];
                  	?>

                    <div class="col-sm-offset-2 col-sm-10">
                       <label>
                          Note: This is the code you use to get your exam's result
                        </label>
                    </div>
                  	<div class="form-group">
	                    <label for="inputSCode" class="col-sm-2 control-label">Student Code</label>
	                    <div class="col-sm-10">
	                      <input name="scode" type="number" class="form-control" id="inputSCode" value="<?php echo $SCode; ?>">
	                    </div>
                  	</div>

                  	<div class="form-group">
                  </div>

                  <!-- Choose Courses you study -->
                  <?php
					         $exceptions = array("Break1","Break2","Break3","Break4","Lab","lab2","Lab1-Sec1","Lab1-Sec2",
						                            "PRJ1","PRJ2","PRJ3","PRJ4","Lab3-Sec1","Lab3-Sec2","Lab4-Sec1","Lab4-Sec2");
                  	$sql = "SELECT Year FROM student WHERE StudentID = $data[0]";
                  	$year = "";
                  	$query = mysqli_query($conn,$sql);
                  	$year = mysqli_fetch_assoc($query)['Year'];
                  	$select = "SELECT courseName,courseCode FROM courses WHERE year='$year'";
                  	$courses = mysqli_query($conn,$select);
                    $attachedCourses = array();
                    $selectAtt = mysqli_query($conn,"select CourseCode from users_courses where UserID = $ID");
                    while ($row = mysqli_fetch_array($selectAtt))
                      array_push($attachedCourses, $row['CourseCode']); ?>
                  	<div style="margin-left:5%">
                    <div class="form-group">
  	                  <h3>Courses</h3>
                    </div>
                    	<?php 
                    	$i = 1;
                    	while ($row = mysqli_fetch_assoc($courses)) {
                    		if (in_array($row['courseCode'], $exceptions))
                    			continue;
                    	?>
  	                    <div class="form-group">
  	                      <input name="course[]" type="checkbox" value="<?php echo $row['courseCode'];?>" <?php if(in_array($row['courseCode'], $attachedCourses)) echo 'checked'?>>
  	                      <label><?php echo $row['courseName'] ?></label>
  	                    </div>
                    	<?php }
                    	?>
                    </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button  name="Update_Info" type="submit" class="btn btn-danger">Update</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="changepass">

              <?php 
              $UserID = $_SESSION['Student']['StudentID'];
              $Student_Data = "SELECT * FROM user WHERE UserID = '$UserID'";
              $data = mysqli_fetch_row($conn->query($Student_Data));
              

              ?>
                <form  id="js-ajax-php-json" class="form-horizontal js-ajax-php-json" action="update_pass.php" method="post">
                  
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Password</label>

                    <div class="col-sm-10">
                      <input name="oldpass" type="password" required class="form-control" id="pass1" placeholder="Enter Old password">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Password</label>

                    <div class="col-sm-10">
                      <input name="pass" type="password" required class="form-control" id="pass2" placeholder="Enter New password">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button  name="Update_Info" type="submit" class="btn btn-danger">Update</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->


            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->



        </div>
        <!-- /.col -->
      </div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; CMP-Notifier 2015-2016 <a href="#">CMP Notifier</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>         
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->



          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

  <?php include('scripts.php');?>    
  <!-- Notification after update password-->



  <script type='text/javascript'>
    /* attach a submit handler to the form */
    $("#js-ajax-php-json").submit(function(event) {

      /* stop form from submitting normally */
      event.preventDefault();

      /* get some values from elements on the page: */
      var $form = $( this ),
          url = $form.attr( 'action' );

      /* Send the data using post */
      var posting = $.post( url, { oldpass: $('#pass1').val(), pass: $('#pass2').val() } );

      /* Alerts the results */
      posting.done(function( data ) {
        console.log(data);
        var obj = jQuery.parseJSON( data );
       if (obj.state == "success"){
      var text = '<div class="activity-item"> <i style="font-size: 65px;float: left;color: #3A823A;" class="fa fa-shopping-cart text-success"></i> <div class="activity" style="float: left;margin-left: 14px;font-size: 17px;margin-top: 21px;"> Password Changed Successfully </div> </div>';
      generate('success', text);
      }
      else if(obj.state == "oldpassfail"){
      var text = '<div class="activity-item"> <i style="font-size: 65px;float: left;color: #3A823A;" class="fa fa-shopping-cart text-success"></i> <div class="activity" style="float: left;margin-left: 14px;font-size: 17px;margin-top: 21px;"> old password is wrong </div> </div>';
      generate('warning', text);
      }
      else{
      var text = '<div class="activity-item"> <i style="font-size: 65px;float: left;color: #3A823A;"  class="fa fa-shopping-cart text-success"></i> <div class="activity" style="float: left;margin-left: 14px;font-size: 17px;margin-top: 21px;"> Error try again later </div> </div>';
        generate('fail', text);
      }
      });
    });
</script>


<script type="text/javascript">


        function generate(type, text) {

            var n = noty({
                text        : text,
                type        : type,
                dismissQueue: true,
                layout      : 'topRight',
                closeWith   : ['click'],
                theme       : 'relax',
                maxVisible  : 10,
                animation   : {
                    open  : 'animated bounceInLeft',
                    close : 'animated bounceOutLeft',
                    easing: 'swing',
                    speed : 500
                }
            });
            console.log('html: ' + n.options.id);
        }

    </script>
  </body>
</html>


