          <div class="navbar-custom-menu">
            <audio id="aud" preload="auto">
              <source src="http://cmpnotifier.net/modules/assests/notification/messagealert.mp3" type="audio/mpeg">
            </audio>
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <?php //to get the num of unseen notifications 
                  if(isset($_SESSION['Student']))
                  {
                    $userid = $_SESSION['Student']['StudentID'];
                  }
                  elseif(isset($_SESSION['Doctor']))
                  {
                    $userid = $_SESSION['Doctor']['TeachingID'];
                  } 
              ?>
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="loadmessages()">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success" id="unseenmessage"></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header" id="msgheader"></li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu" id="msgmenu">
                    

                    </ul>
                  </li>
                  <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
              <?php if(isset($_SESSION['Student'])){ ?>
              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="loadannounce()">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning" id="unseenannounce"></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header" id="announceheader"></li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                      <ul class="menu" id="announcemenu">
                      
                      </ul>

                  </li>
                  <li class="footer"><a href="#">View all</a></li>
                </ul>
              </li>
              <!-- Tasks: style can be found in dropdown.less -->
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="loadvotes()">
                  <i class="fa fa-question-circle"></i>
                  <span class="label label-danger" id="unansvotes"></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header" id="votesheader"></li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu" id="votesmenu">

                      
                    </ul>
                  </li>
                  <li class="footer">
                    <a href="#">View all votes</a>
                  </li>
                </ul>
              </li>
              <?php } ?>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?php 
              if(isset($_SESSION['Doctor']))
              {
                echo '<img style="width:18px; height:18px;  margin-right:5px;" src="'.$_SESSION['Doctor']['Image'].'" class="img-circle" alt="User Image">';
                echo '<span class="hidden-xs">'.$_SESSION["Doctor"]["UserName"].'</span>';
              }
              else
              {
                echo '<img style="width:18px; height:18px;  margin-right:5px;" src="'.$_SESSION['Student']['Image'].'" class="img-circle" alt="User Image">';
                echo '<span class="hidden-xs">'.$_SESSION["Student"]["UserName"].'</span>';
              }
              ?> 


                  
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
             <?php 
              if(isset($_SESSION['Doctor']))
              {
                echo '<img style="width:90px; height:90px;  margin-right:5px;" src="'.$_SESSION['Doctor']['Image'].'" class="img-circle" alt="User Image">';
               
              }
              else
              {
                echo '<img style="width:90px; height:90px;  margin-right:5px;" src="'.$_SESSION['Student']['Image'].'" class="img-circle" alt="User Image">';
                
              }
              ?>

                    <p>
                      <?php 
                      if(isset($_SESSION['Doctor']))
                      {
                        echo $_SESSION["Doctor"]["UserName"];  
                      }
                      else
                      {
                        echo $_SESSION["Student"]["UserName"];  
                      }
                      
                      ?>
                      <small>Last Login yesterday</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="/" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="/logout.php"  class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>


<script>
$(document).on("click", ".open-MessageDialog", function() {
     $('#ann_id').val($(this).data('id'));     
});
</script>

<script>
if(typeof(EventSource) !== "undefined") {
    var announcesource = new EventSource("/checknewannounce.php?userid=<?php echo $userid; ?>");
    announcesource.onmessage = function(event) {
        document.getElementById("unseenannounce").innerHTML = event.data ;
        document.getElementById("announceheader").innerHTML = "You have "+ event.data +" new announcements";
    };
} else {
    alert("Sorry, your browser does not support server-sent events...");
}
    var messagesource = new EventSource("/checknewmessages.php?userid=<?php echo $userid; ?>");
    var oldnum = document.getElementById("unseenmessage").innerHTML;
    messagesource.onmessage = function(event) {
        document.getElementById("unseenmessage").innerHTML = event.data ;
        document.getElementById("msgheader").innerHTML = "You have "+ event.data +" new messages";
        if((document.getElementById("unseenmessage").innerHTML - oldnum) == 1){
          document.getElementById("aud").play();
          oldnum = document.getElementById("unseenmessage").innerHTML;
        }
    };
    var votessource = new EventSource("/checknewvotes.php?userid=<?php echo $userid; ?>");
    votessource.onmessage = function(event) {
        document.getElementById("unansvotes").innerHTML = event.data ;
        document.getElementById("votesheader").innerHTML = "You have "+ event.data +" new votes";
    };
</script>

<script type="text/javascript">
    function loadannounce(){
          $("#announcemenu").load("/getnewannounce.php?userid=<?php echo $userid; ?>");
  }
  function loadmessages(){
          $("#msgmenu").load("/getnewmessages.php?userid=<?php echo $userid; ?>");
  }
  function loadvotes(){
          $("#votesmenu").load("/getnewvotes.php?userid=<?php echo $userid; ?>");
  }
  </script>