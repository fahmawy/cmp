
<?php 
  if(!isset($_SESSION)){
      session_start();
  }

  include_once("../config.php");
  include_once("../function.php");

  if(!isset($_SESSION['Student']))
  { 
      header("Location: /index.php");
  }
?>
<!DOCTYPE html>
<html>
  <?php include('../header.php');?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>C</b>MP</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>CMP</b> Notifier</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
            <?php include('../custom-nav_bar.php');?>
        </nav>
      </header>
      <!-- Left side column. conntains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="/modules/assests/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <!--<p><?php /*echo $_SESSION["Doctor"]["UserName"]*/;?></p>-->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
          <?php  include ('../menu.php'); ?>        
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- conntent Wrapper. conntains page conntent -->
      <div class="conntent-wrapper">
        <!-- conntent Header (Page header) -->
        <section class="conntent-header">
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Article</li>
          </ol>
        </section>

        <!-- Main conntent -->
      <section class="conntent"> 
      <br>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">
                <?php
                 $projectID = $_GET['PID'];
                 $getTitle = $mysqli_query($conn,"SELECT `title` FROM `project` WHERE `project`.ProjectID = $projectID");
                 if($getTitle){
                      $row = mysqli_fetch_array($getTitle);
                      $title = $row['title'];
                      echo $title;
                 }else{
                    header("Location: /index.php");
                 }
                ?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">
            <?php
                $StudentID = $_SESSION['Student']['StudentID'];
                $query = mysqli_query($conn,"SELECT `student_teams`.`TeamID`, `student_teams`.`TeamLeaderID`, `student_teams`.`Team_Grade` FROM `student_teams` WHERE `student_teams`.`StudentID` = $StudentID AND `student_teams`.`ProjectID`= $projectID");
                if($query){
                  if(mysqli_num_rows($query) > 0){
                    $row = mysqli_fetch_array($query);
                    echo "<h1>The Team Code :". $row['TeamID']."<h1>";

                    //View Team Grade
                    if ($row['Team_Grade'] != NULL)
                      echo "<h2>Team Grade : ".$row['Team_Grade']."<h2>";
                    else
                      echo "<h2>Team Grade hasn't been submitted yet</h2>";

                    //View Tasks
                    //Add Task to the team members for the team leader
                    if($row['TeamLeaderID']==$StudentID)
                      echo "<a href='addtask.php?PID=" . $projectID . "'>Add Tasks to your teammates</a>";
                    
                    //View Tasks to the whole team members
                    echo"<h1>Tasks: </h1>";
                  $tasksRows = mysqli_query($conn,"select conntent,AnnID from announcements,student_teams where Type = 'task' and ProjectID = $projectID and reciever = StudentID and reciever =$StudentID");
                  while ($rowTask=mysqli_fetch_array($tasksRows)) {
                    $annID = $rowTask['AnnID'];
                    $conntent = $rowTask['conntent'];
                    //determine weather the user saw it or not
                    $seen = 'white';  //default
                    $seenq = mysqli_query($conn,"select * from not_seen_announce where UserID = $StudentID and AnnounceID = $annID");
                    if(mysqli_num_rows($seenq)>0)
                      $seen = 'lightgray';
                    //printing the table
                    echo"<h2 style='background-color:".$seen."'>".$conntent."</h2>";
                  }
                  //Add Announcement to the team leader only
                  if($row['TeamLeaderID']){
                    if (isset($_POST['Submit'])){
                      $connError = "";
                      $conntent = "";
                      if(empty($_POST['conntent']))
                        $connError="Enter the conntent please";
                      else{
                        $conntent = $_POST['conntent'];
                        $TeamID = $row['TeamID'];
                        //add to announcement table
                        $insert = mysqli_query($conn,"insert into announcements (SenderID,IsShared,Type,conntent,Reciever) values ($StudentID,true,'teamAnnounce','$conntent',$TeamID)");
                        $AnnounceID = mysqli_insert_id($conn);
                        //add the target to the unseen table
                        $targent = mysqli_query($conn,"select StudentID from student_teams where TeamLeaderID = '$StudentID' and studentID !='$StudentID'");
                        if($targent){
                          $unseeninsert = "insert into not_seen_announce (userID,AnnounceID) values ";
                          while ($row=mysqli_fetch_assoc($targent)){
                            $MemberID = $row['StudentID'];
                            $unseeninsert .= "($MemberID,$AnnounceID),";
                          }
                          $unseeninsert = substr($unseeninsert,0,-1);
                          $unseeninsert .=";";
                          $query = mysqli_query($conn,$unseeninsert);
                        }else{
                          echo "<script>window.alert('error while inserting the announcement');</script>";
                          $delete = mysqli_query($conn,"DELETE FROM `announcements` WHERE AnnID = '$AnnounceID'");
                        }
                      }
                    }
                    if($row['TeamLeaderID']==$StudentID){
                      $form ="<h2>Send Announcement to your team members</h2>
                            <form method='post'>
                          <textarea name='conntent' placeholder=Announcement conntent' value='";
                      if(isset($conntent))
                        $form .= $conntent;
                      $form .= "'></textarea>
                          <p>";
                      if(isset($connError))
                        $form .= $connError;
                       $form .= "</p>
                          <input type='submit' value='Send Announcement' name='Submit'>
                            </form>";
                      echo $form;
                    }
                    //send announcement to the team leader from the team Leader
                    else{
                      if (isset($_POST['Submit1'])){
                        if(empty($_POST['msgconntent'])){
                          $msgconnError="Enter the conntent please";
                          }
                        else{
                          $msgconntent=$_POST['msgconntent'];
                          $TeamLeaderID = $row['TeamLeaderID'];
                          $insert = mysqli_query($conn,"insert into announcements (SenderID,IsShared,Type,conntent,reciever) values ($StudentID,false,'message','$msgconntent',$TeamLeaderID)");
                            //add the target to the unseen table
                            $AnnounceID = mysqli_insert_id($conn);
                            if($insert){
                              $unseeninsert = "insert into not_seen_announce (userID,AnnounceID) values ($TeamLeaderID,$AnnounceID)";
                              $query = mysqli_query($conn,$unseeninsert);
                              echo "<script>window.alert('Done');</script>";
                            }
                            else{
                              echo "<script>window.alert('error while sending the message');</script>";
                              $delete = "delete from not_seen_announce where AnnounceID=$AnnounceID";
                            }
                        }
                      }
                      $form ="<h2>Send Announcement to your team Leader</h2>
                          <form method='post'>
                          <textarea name='msgconntent' placeholder='Message conntent' value='";
                      if(isset($msgconntent))
                        $form .= $msgconntent;
                      $form .= "'></textarea>
                          <p>";
                      if(isset($msgconnError))
                        $form .= $msgconnError;
                       $form .= "</p>
                          <input type='submit' value='Send Message' name='Submit1'>
                            </form>";
                      echo $form;
                    }
                  }else{
                    echo $row['TeamLeaderID'];
                  }
                }else
                  echo "<h1>Wrong ID</h1>";
                }else
                  echo "<h1>Error Has Occured</h1>";
        		?>
            </div>

          </div>

        </div>



      </div>     


      <?php
             
        if ($connn->connnect_error) {
            die("connnection failed: " . $connn->connnect_error);
        }
        else
        {
           echo "connnected successfully"; 
           
           
        } 

       ?>

        </section><!-- /.conntent -->
      </div><!-- /.conntent-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; CMP-Notifier 2015-2016 <a href="#">CMP Notifier</a>.</strong> All rights reserved.
      </footer>

      <!-- conntrol Sidebar -->
      <aside class="conntrol-sidebar conntrol-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified conntrol-sidebar-tabs">
          <li><a href="#conntrol-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>         
        </ul>
        <!-- Tab panes -->
        <div class="tab-conntent">
          <!-- Home tab conntent -->
          <div class="tab-pane" id="conntrol-sidebar-home-tab">
            <h3 class="conntrol-sidebar-heading">Recent Activity</h3>
            <ul class="conntrol-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-iconn fa fa-birthday-cake bg-red"></i>
                  
                  <div class="menu-info">
                    <h4 class="conntrol-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.conntrol-sidebar-menu -->



          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.conntrol-sidebar -->
      <div class="conntrol-sidebar-bg"></div>
    </div><!-- ./wrapper -->

  <?php include('../scripts.php');?>    
  </body>
</html>
