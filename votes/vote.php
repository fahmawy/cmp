<?php 

	if(!isset($_SESSION)){
	    session_start();
	}
	include('../config.php');
	$vID = $_GET['vID'];
	$questionq = mysqli_query($conn,"select MakerID,Question,Expire_Date from votes where VotingID = $vID");
	$questionq = mysqli_fetch_assoc($questionq);
	$question = $questionq['Question'];
	$makerid = $questionq['MakerID'];
	$expiredate = $questionq['Expire_Date'];

	$studentnamequery = mysqli_query($conn,"select Name from user where UserID = $makerid");
	$studentnamequery = mysqli_fetch_assoc($studentnamequery);
	$studentname = $studentnamequery['Name'];
	$allowedanswerdata = mysqli_query($conn,"select Allowed_Answer from allowed_answers where VotingID = $vID");
	
	$expired = false;
	if($expiredate <= date('Y-m-d')){
		echo "<script>window.alert('error the vote is expired');</script>";
		$expired = true;
	}

	//check if the user answered before
	$uid = $_SESSION['userID'];
	$preanswerq = mysqli_query($conn,"select SubAns from submitted_answers where VotingID = $vID and UserID = $uid");
	if(mysqli_num_rows($preanswerq)>0){
		$preanswerq = mysqli_fetch_assoc($preanswerq);
		$preanswer = $preanswerq['SubAns'];
	}
	else
		$preanswer = null;
	//process request
	if (isset($_POST['submit'])){
		$answer = $_POST['answer'];
		//add to announcement table
		if($preanswer == null){
			$insert = mysqli_query($conn,"insert into submitted_answers (VotingID,UserID,SubAns) values ($vID,$uid,'$answer')");
			$update = null;
		}
		else{
			$update = mysqli_query($conn,"update submitted_answers set SubAns = '$answer' where VotingID = $vID and UserID = $uid");
			$insert = null;
		}
		//remove the user from the not_ans_votes
		if($insert){
			$unvoteddel = "delete from not_ans_votes where userID = $uid and VotingID = $vID";
			$query = mysqli_query($conn,$unvoteddel);
			echo "<script>window.alert('Done :D');</script>";
		}
		else if ($update)
			echo "<script>window.alert('Done :D');</script>";
		else
			echo "<script>window.alert('error while sending the message');</script>";
	}	
	//check if the user answered before
	$uid = $_SESSION['userID'];
	$preanswerq = mysqli_query($conn,"select SubAns from submitted_answers where VotingID = $vID and UserID = $uid");
	if(mysqli_num_rows($preanswerq)>0){
		$preanswerq = mysqli_fetch_assoc($preanswerq);
		$preanswer = $preanswerq['SubAns'];
	}
	else
		$preanswer = null;

?>
<!doctype html>
<html>
<head>
	<title>View Vote</title>
</head>
<body>
	<h1 style="text-align:center">View Vote</h1>
	<?php
		if(!$expired){
	?>
			<form method="POST">
				<p><?php echo $studentname; ?> asked :</p>
				<p><?php echo $question; ?></p>
				<?php
					while ($row=mysqli_fetch_assoc($allowedanswerdata)) {
						if($row['Allowed_Answer'] == $preanswer){
						?>
						<!-- Create the answers with the value of the answer and Display it -->
						<input type="radio" name="answer" value="<?php echo $row['Allowed_Answer']; ?>" checked><?php echo $row['Allowed_Answer']; ?>
						<?php
						}
						else{
							?>
							<input type="radio" name="answer" value="<?php echo $row['Allowed_Answer']; ?>"><?php echo $row['Allowed_Answer']; ?>
							<?php
						}
					}
					echo '<input type="submit" value="Submit" name="submit">';
					echo '</form>';
				}
			else{
				?>
				<p><?php echo $studentname; ?> asked :</p>
				<p><?php echo $question; ?></p>
				<table border="1" style="text-align:center">
					<tr>
						<th>Answer</th>
						<th># Votes</th>
						<th>View Details</th>
					</tr>
				<?php
					$answersdata = mysqli_query($conn,"SELECT allowed_answers.Allowed_Answer,COUNT(submitted_answers.SubAns) AS NumberOfAns FROM submitted_answers right join allowed_answers on allowed_answers.Allowed_Answer = submitted_answers.SubAns where allowed_answers.VotingID = $vID GROUP BY Allowed_Answer");
					while ($row=mysqli_fetch_assoc($answersdata)) {
						?>
							<tr>
								<td><?php echo $row['Allowed_Answer']; ?></td>
								<td><?php echo $row['NumberOfAns']; ?></td>
								<td><a href="ansdetails.php?vID=<?php echo $vID; ?>&ans=<?php echo $row['Allowed_Answer']; ?>">View Details</a></td>
							</tr>
						<?php
					}
				?>
				</table>
			<?php
			}
			?>
</body>
</html>