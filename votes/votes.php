<?php 

  if(!isset($_SESSION)){
      session_start();
  }

  include_once("../config.php");
  include_once("../function.php");

  if(!isset($_SESSION['Student']))
  { 
      header("Location: /index.php");
  }

  else{
    $UserName = $_SESSION["Student"]["UserName"];
    $image = $_SESSION["Student"]["Image"];
    $year = $_SESSION['Student']['Year'];
  }

?>
<!DOCTYPE html>
<html>
  <?php include('../header.php');?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>C</b>MP</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>CMP</b> Notifier</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <?php include('../custom-nav_bar.php');?>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo $image; ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $UserName;?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
          <?php  include ('../menu.php'); ?>         
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Course</li>
          </ol>
        </section>

        <!-- Main content -->
      <section class="content"> 
      <br>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Add Vote</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div>

            <div class="box-body" style="display: block;">

            <?php 

  if(!isset($_SESSION)){
      session_start();
  }
  include('../config.php');
  $year = $_SESSION['Student']['Year'];
  $questionq = mysqli_query($conn,"select MakerID,Question,Expire_Date from votes where Reciever = $year");
  $questionq = mysqli_fetch_assoc($questionq);
  $question = $questionq['Question'];
  $makerid = $questionq['MakerID'];
  $expiredate = $questionq['Expire_Date'];

  $studentnamequery = mysqli_query($conn,"select Name from user where UserID = $makerid");
  $studentnamequery = mysqli_fetch_assoc($studentnamequery);
  $studentname = $studentnamequery['Name'];
  $allowedanswerdata = mysqli_query($conn,"select Allowed_Answer from allowed_answers where VotingID = $vID");
  
  $expired = false;
  if($expiredate <= date('Y-m-d')){
    echo "<script>window.alert('error the vote is expired');</script>";
    $expired = true;
  }

  //check if the user answered before
  $uid = $_SESSION['userID'];
  $preanswerq = mysqli_query($conn,"select SubAns from submitted_answers where VotingID = $vID and UserID = $uid");
  if(mysqli_num_rows($preanswerq)>0){
    $preanswerq = mysqli_fetch_assoc($preanswerq);
    $preanswer = $preanswerq['SubAns'];
  }
  else
    $preanswer = null;
  //process request
  if (isset($_POST['submit'])){
    $answer = $_POST['answer'];
    //add to announcement table
    if($preanswer == null){
      $insert = mysqli_query($conn,"insert into submitted_answers (VotingID,UserID,SubAns) values ($vID,$uid,'$answer')");
      $update = null;
    }
    else{
      $update = mysqli_query($conn,"update submitted_answers set SubAns = '$answer' where VotingID = $vID and UserID = $uid");
      $insert = null;
    }
    //remove the user from the not_ans_votes
    if($insert){
      $unvoteddel = "delete from not_ans_votes where userID = $uid and VotingID = $vID";
      $query = mysqli_query($conn,$unvoteddel);
      echo "<script>window.alert('Done :D');</script>";
    }
    else if ($update)
      echo "<script>window.alert('Done :D');</script>";
    else
      echo "<script>window.alert('error while sending the message');</script>";
  } 
  //check if the user answered before
  $uid = $_SESSION['userID'];
  $preanswerq = mysqli_query($conn,"select SubAns from submitted_answers where VotingID = $vID and UserID = $uid");
  if(mysqli_num_rows($preanswerq)>0){
    $preanswerq = mysqli_fetch_assoc($preanswerq);
    $preanswer = $preanswerq['SubAns'];
  }
  else
    $preanswer = null;

?>
  <h1 style="text-align:center">View Vote</h1>
  <?php
    if(!$expired){
  ?>
      <form method="POST">
        <p><?php echo $studentname; ?> asked :</p>
        <p><?php echo $question; ?></p>
        <?php
          while ($row=mysqli_fetch_assoc($allowedanswerdata)) {
            if($row['Allowed_Answer'] == $preanswer){
            ?>
            <!-- Create the answers with the value of the answer and Display it -->
            <input type="radio" name="answer" value="<?php echo $row['Allowed_Answer']; ?>" checked><?php echo $row['Allowed_Answer']; ?>
            <?php
            }
            else{
              ?>
              <input type="radio" name="answer" value="<?php echo $row['Allowed_Answer']; ?>"><?php echo $row['Allowed_Answer']; ?>
              <?php
            }
          }
          echo '<input type="submit" value="Submit" name="submit">';
          echo '</form>';
        }
      else{
        ?>
        <p><?php echo $studentname; ?> asked :</p>
        <p><?php echo $question; ?></p>
        <table border="1" style="text-align:center">
          <tr>
            <th>Answer</th>
            <th># Votes</th>
            <th>View Details</th>
          </tr>
        <?php
          $answersdata = mysqli_query($conn,"SELECT allowed_answers.Allowed_Answer,COUNT(submitted_answers.SubAns) AS NumberOfAns FROM submitted_answers right join allowed_answers on allowed_answers.Allowed_Answer = submitted_answers.SubAns where allowed_answers.VotingID = $vID GROUP BY Allowed_Answer");
          while ($row=mysqli_fetch_assoc($answersdata)) {
            ?>
              <tr>
                <td><?php echo $row['Allowed_Answer']; ?></td>
                <td><?php echo $row['NumberOfAns']; ?></td>
                <td><a href="ansdetails.php?vID=<?php echo $vID; ?>&ans=<?php echo $row['Allowed_Answer']; ?>">View Details</a></td>
              </tr>
            <?php
          }
        ?>
        </table>
      <?php
      }
      ?>

            </div>

          </div>

        </div>



      </div>     


      <?php
             
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }        

       ?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; CMP-Notifier 2015-2016 <a href="#">CMP Notifier</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>         
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->



          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

  <?php include('../scripts.php');?>   

    <script type="text/javascript">
    var counter = 1;
    function addInput(divName){
          var newdiv = document.createElement('div');
          newdiv.innerHTML = "<br><input id='newin' class='form-control' type='text' name='answers[]' placeholder='Allowed answer'>";
          document.getElementById(divName).appendChild(newdiv);
          counter++;
  }
  </script>


  </body>
</html>
