<?php 
	if(!isset($_SESSION)){
	    session_start();
	}
?>
<!doctype html>
<html>
<head>
	<title>Announcements</title>
	<script type="text/javascript">
		var counter = 1;
		function addInput(divName){
          var newdiv = document.createElement('div');
          newdiv.innerHTML = "<br><input id='newin' type='text' name='answers[]' placeholder='Allowed answer'>";
          document.getElementById(divName).appendChild(newdiv);
          counter++;
	}
	</script>
</head>
<body>
	<div id="Vote">
		<h2>Make a poll question! </h2>
		<form method="POST">
			Question: <input type="text" name="question"><br>
			Reciver:
			<select name="receiver">
				<option value="">-</option>
				<option value="1">CMP 1</option>
				<option value="2">CMP 2</option>
				<option value="3">CMP 3</option>
				<option value="4">CMP 4</option>
				<option value="5">All</option>
			</select><br>
			Expire Date: <input type="date" name="ex-date">	<br>
			Allowed Answers:
			<div id="newInput">
			<p>*Write one answer and for more answers click on "Add another answer"</p>
          	<input type="text" name="answers[]" size="30" placeholder="Allowed answer"> 
     		</div>
			<input type="button" value="Add answer" onClick="addInput('newInput');"><br>
			<input type="submit" name="submit" value="Submit"><br>
		</form>
	</div>
</body>
</html>
<?php
	if (isset($_POST['submit'])){
		include('../config.php');
		$uid = $_SESSION['userID'];
		$questErr = $recErr = $ansErr = "" ;
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$question = $_POST['question'];
		$receiver = $_POST['receiver'];
		$answers = $_POST["answers"];
		$all_answers = implode(",", $answers);
		$ex_date = $_POST['ex-date'];
		//Question Validation
		if(empty($content))
			$contentErr = "Please enter your question!";
		else
			$contentErr ="";
		//Receiver Validation
		if(empty($receiver))
			$recErr = "Receiver is required";
		else
			$recErr ="";

		//Answers Validation
		if (empty($all_answers))
			$ansErr = "Enter At least one answer!";
		// Make sure no dupliactes are there
			$query = mysqli_query($conn, "SELECT Question FROM votes WHERE Question='$question'");
			if ($query){
				if (mysqli_num_rows($query) == 0) {
					mysqli_set_charset($conn, 'utf8');
					$insert= mysqli_query($conn,"INSERT INTO votes (
																		MakerID,
																		Question,
																		Expire_Date,
																		Reciever
																	) VALUES (
																		'$uid',
																		'$question',
																		'$ex_date',
																		'$receiver'					
																	)");
					$voteid = mysqli_insert_id($conn);
					$text = "INSERT INTO allowed_answers (VotingID,Allowed_Answer) VALUES ";
					foreach ($answers as $one) {
						$text .= "('$voteid','$one'),";
					}
					$text = substr($text, 0,-1);
					$text.= ";";
					$insert_ans = mysqli_query($conn,$text);

					//add to announcement table
					$content = "../votes/vote.php?vID=$voteid";
					$insert_ann = mysqli_query($conn,"insert into announcements (SenderID,IsShared,Type,Content,Reciever) values ($uid,true,'vote','$content',$receiver)");
					$AnnounceID = mysqli_insert_id($conn);
					//add the target to the unseen table and unanswer table
					if($receiver !=5)
						$targent = mysqli_query($conn,"select StudentID from student where Year = '$receiver'");
					else
						$targent = mysqli_query($conn,"select StudentID from student");
					if($targent){
						$unseeninsert = "insert into not_seen_announce (userID,AnnounceID) values ";
						$unansinsert = "insert into not_ans_votes (userID,VotingID) values ";
						while ($row=mysqli_fetch_assoc($targent)){
							$StudentID = $row['StudentID'];
							$unseeninsert .= "($StudentID,$AnnounceID),";
							$unansinsert .= "($StudentID,$voteid),";
						}
						$unseeninsert = substr($unseeninsert,0,-1);
						$unseeninsert .=";";
						$unansinsert = substr($unansinsert,0,-1);
						$unansinsert .=";";

						$query = mysqli_query($conn,$unseeninsert);
						$query = mysqli_query($conn,$unansinsert);
					
					if(!$insert || !$insert_ans) {
						echo 'Insert Error: '.mysqli_error($conn);
						$_SESSION['msg'] = "<div class='regmsg fail'>Something went wrong, please answer again!</div>";
					} else {
						setcookie('votes');
						$_SESSION['msg'] = "<div class='regmsg success'>Your data have successfully been sumbitted. Thanks for your time.</div>";
					}
				} else {
					$_SESSION['msg'] = "<div class='regmsg fail'>You have filled this before.</div>";
				}
			} else {
				$_SESSION['msg'] = "<div class='regmsg fail'>Something went wrong, please try again!</div>";
			}
		}
	}
}
?>