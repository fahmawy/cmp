<!-- jQuery 2.1.4 -->
    <script src="/modules/assests/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="/modules/assests/bootstrap/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="/modules/assests/plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="/modules/assests/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="/modules/assests/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="/modules/assests/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="/modules/assests/plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="/modules/assests/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="/modules/assests/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="/modules/assests/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="/modules/assests/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="/modules/assests/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/modules/assests/dist/js/app.min.js"></script>
    <script src="/modules/assests/dist/js/pages/dashboard.js"></script>
    <script src="/modules/assests/dist/js/demo.js"></script>



    <script type="text/javascript" src="/modules/assests/notification/jquery.noty.packaged.min.js"></script>