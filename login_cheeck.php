<?php
	
  session_start();

  include_once("config.php");
  include_once("function.php");

  if(!isset($_SESSION['Student']) || !isset($_SESSION['Doctor']))
  { 
      header("Location: index.php");
  }
  else
  {
  	if(isset($_SESSION['Student']))
  	{
  		header("Location: student.php");
  	}
  	elseif(isset($_SESSION['Doctor']))
  	{
  		header("Location: doctor.php");
  	}
  }

	if( isset($_POST['submit']) )
	{
		$user_name = $_POST['user_name'] ; 
		$user_pass = md5($_POST['user_pass']);		
		  //pr($user_pass);exit;
			$Exist = "SELECT UserID FROM user WHERE Email ='$user_name' AND Password = '$user_pass'";
		    $UserID = $conn->query($Exist);

		    if( $UserID ->num_rows == 1)
		    {
		    	$row = mysqli_fetch_row($UserID);
		    	$UserID = implode("",$row);		    	
		    	
		    	$Role = "SELECT IsStudent FROM user WHERE UserID ='$UserID'";			    	
		    	$data = mysqli_fetch_row($conn->query($Role));
		    	$Student = implode("",$data);

		    	$name = "SELECT Name FROM user WHERE UserID ='$UserID'";			    	
		    	$User_Name = implode("",mysqli_fetch_row($conn->query($name)));

		    	$mail = "SELECT Email FROM user WHERE UserID ='$UserID'";			    	
		    	$User_mail = implode("",mysqli_fetch_row($conn->query($mail)));


		    	if($Student)
		    	{
		    		$Student_Data = "SELECT * FROM student WHERE StudentID = '$UserID'";
		    		$data = mysqli_fetch_row($conn->query($Student_Data));

		   			$User_Photo = "SELECT Photo FROM user WHERE UserID = '$UserID'";
           			$Image = mysqli_fetch_row($conn->query($User_Photo));
		    		
		    		if(empty($Image[0]))
		    		{
		    			$Image[0] = "/modules/assests/Images/user.png";
		    		}
		    		
		    		$Student_Data = array(

		    						'UserName'    => $User_Name,
								    'StudentID'   => $data[0],
								    'StudentCode' => $data[1],
								    'Year'        => $data[2],
								    'Section'     => $data[3],								    
								    'BN'          => $data[4],
								    'IsRep'       => $data[5],
								    'ExamBN'      => $data[6],
								    'Image'		  => $Image[0],
								    'Email'       => $User_mail

								  );


		    		$_SESSION["Student"] = $Student_Data;
		    		echo '<meta http-equiv="refresh" content="0; url=student.php" />';
		    	}
		    	else
		    	{
		    		$Doc_Data = "SELECT * FROM teaching_staff WHERE TeachingID = '$UserID'";
		    		$data = mysqli_fetch_row($conn->query($Doc_Data));

		    	    $User_Photo = "SELECT Photo FROM user WHERE UserID = '$UserID'";
           			$Image = mysqli_fetch_row($conn->query($User_Photo));
		    		
		    		if(empty($Image[0]))
		    		{
		    			$Image[0] = "/modules/assests/Images/user.png";
		    		}
		    		$Doctor_Data = array(

		    					   'UserName'   => $User_Name,
							       'TeachingID' => $data[0],
							       'IsDoctor'   => $data[1],
							       'Image'		  => $Image[0]							           
							       );

		    		$_SESSION["Doctor"] = $Doctor_Data;		    		
		    		echo '<meta http-equiv="refresh" content="0; url=doctor.php" />';
		    	}


		    	
		    }
		    else
		    {
		    	echo "This User Is not Exist";
		    }
		    

	} 


?>