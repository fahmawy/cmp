<?php 
  
  if(!isset($_SESSION))
    {
      session_start();
    }
  include_once("config.php");
  include_once("function.php");

  if(!isset($_SESSION['Doctor']))
  { 
      header("Location: index.php");
  }


?>
<!DOCTYPE html>
<html>
  <?php include('header.php');?>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>C</b>MP</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>CMP</b> Notifier</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <?php include('custom-nav_bar.php');?>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img style="width:40px; height:40px;" src="<?php echo $_SESSION['Doctor']['Image'];?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION["Doctor"]["UserName"];?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <?php  include ('menu.php'); ?>           
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->


        <!-- Main content -->
      <section class="content">      
      <?php
              // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        else
        {
           //echo "Connected successfully"; 
           //pr($_SESSION["Doctor"]);

          $ID = $_SESSION['Doctor']['TeachingID'];

           $Title = "SELECT Title FROM user WHERE UserID = $ID";
           $User_Title = mysqli_fetch_row($conn->query($Title));

           $get_bio = "SELECT Bio FROM user WHERE UserID = $ID";
           $Bio = mysqli_fetch_row($conn->query($get_bio));
           
        } 

       ?>


<div class="row">
<!-- Statistics-->

		<?php 
		$UserID =  $_SESSION['Doctor']['TeachingID'];		
		$Course = "SELECT * FROM users_courses WHERE UserID = '$UserID'";
		$All_Course = $conn->query($Course);

		
		$get_student = "SELECT * FROM user WHERE IsStudent = 1";
		$student = $conn->query($get_student);

		$get_doctor = "SELECT * FROM user WHERE IsStudent = 0";
		$doctor = $conn->query($get_doctor);

		?>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $All_Course->num_rows;?></h3>

              <p>Your Courses</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="" class="small-box-footer">
              Courses You Teach <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>


        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>53<sup style="font-size: 20px">%</sup></h3>

              <p>Bounce Rate</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $student->num_rows;?></h3>

              <p>Total Student</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="" class="small-box-footer">
              Registered Student <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $doctor->num_rows;?></h3>

              <p>All Doctor</p>
            </div>
            <div class="icon">
              <i class="fa fa-graduation-cap"></i>
            </div>
            <a href="#" class="small-box-footer">
              Registered Doctor <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>

<!--End Statistics-->

        <div class="col-md-4">
          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Bio</strong>

              <p class="text-muted">
                <?php echo $Bio[0];?>
              </p>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-8">
                    <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#Statistics" data-toggle="tab" aria-expanded="true">Statistics</a></li>              
              <li class=""><a href="#settings" data-toggle="tab" aria-expanded="false">Settings</a></li>
              <li class=""><a href="#changepass" data-toggle="tab" aria-expanded="false">Change Password</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="Statistics">
                <!-- Post -->
                <div class="post">
                	     <div class="box box-widget widget-user-2">
				            <!-- Add the bg color to the header using any of the bg-* classes -->
				            <div class="widget-user-header bg-yellow">
				              <div class="widget-user-image">
				                <img class="img-circle" src="<?php echo $_SESSION['Doctor']['Image']; ?>" alt="User Avatar">
				              </div>
				              <!-- /.widget-user-image -->
				              <h3 class="widget-user-username"><?php echo $_SESSION['Doctor']['UserName']; ?></h3>
				              <h5 class="widget-user-desc"><?php echo $User_Title[0];?></h5>
				            </div>
				            <div class="box-footer no-padding">
				              <ul class="nav nav-stacked">

			              <?php 
			              		$selectCourses = mysqli_query($conn,"SELECT CourseCode FROM users_courses WHERE UserID = '$UserID'");
							  	while($Courses = mysqli_fetch_array($selectCourses)) 
							  	{
                    $Course = $Courses['CourseCode'];
                    print_r($Course);
							  		$query = mysqli_query($conn,"SELECT * FROM users_courses,student WHERE CourseCode='$Course' AND  UserID = StudentID");
							  		$students = mysqli_num_rows($query);

					  		        $get_name = "SELECT courseName FROM courses WHERE courseCode = '$Course'";
							        $Course_name = mysqli_fetch_row($conn->query($get_name));		

							  		echo '<li style="font-size: 20px;"><a href="#">'.$Course_name[0].' <span style="margin-top: 3px;" class="pull-right badge bg-blue">'.'Number Of Student ['.$students.']</span></a></li>';
							  		
							  	}
			              ?>
				              </ul>
				            </div>
				          </div>
                </div>
                <!-- /.post -->

                
              </div>
              <!-- /.tab-pane -->
              


              <div class="tab-pane" id="settings">

              <?php 
              $UserID = $_SESSION['Doctor']['TeachingID'];
              $Student_Data = "SELECT * FROM user WHERE UserID = '$UserID'";
              $data = mysqli_fetch_row($conn->query($Student_Data));
              //pr($data);

              ?>
                <form  class="form-horizontal" method="POST" action="update_info.php">
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-10">
                      <input name="usr_name" type="text" disabled class="form-control" id="inputName" value="<?php echo $data[1];?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-10">
                      <input name="email" type="email" class="form-control" id="inputEmail" value="<?php echo $data[4]; ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputExperience" class="col-sm-2 control-label">Biography</label>

                    <div class="col-sm-10">
                      <textarea name="bio" class="form-control" id="inputExperience"><?php echo $data[6];?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Photo</label>

                    <div class="col-sm-10">
                      <input name="photo" type="text" class="form-control" id="inputSkills" value="<?php echo $data[5]; ?>">
                    </div>
                  </div>                            
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                       <label>
                          Note: Enter Your Skills Seperated By , comma
                        </label>
                    </div>
                  </div>

                  <!-- Choose Courses you teach -->
                  <?php
                     $exceptions = array("Break1","Break2","Break3","Break4","Lab","lab2","Lab1-Sec1","Lab1-Sec2",
                                        "PRJ1","PRJ2","PRJ3","PRJ4","Lab3-Sec1","Lab3-Sec2","Lab4-Sec1","Lab4-Sec2");
                    $select = "SELECT courseName,courseCode FROM courses";
                    $courses = mysqli_query($conn,$select);
                    $attachedCourses = array();
                    $selectAtt = mysqli_query($conn,"select CourseCode from users_courses where UserID = $ID");
                    while ($row = mysqli_fetch_array($selectAtt))
                      array_push($attachedCourses, $row['CourseCode']); ?>
                    <div style="margin-left:5%">
                    <div class="form-group">
                      <h3>Courses</h3>
                    </div>
                      <?php 
                      $i = 1;
                      while ($row = mysqli_fetch_assoc($courses)) {
                        if (in_array($row['courseCode'], $exceptions))
                          continue;
                      ?>
                        <div class="form-group">
                          <input name="course[]" type="checkbox" value="<?php echo $row['courseCode'];?>" <?php if(in_array($row['courseCode'], $attachedCourses)) echo 'checked'?>>
                          <label><?php echo $row['courseName'] ?></label>
                        </div>
                      <?php }
                      ?>
                    </div>

                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button  name="Update_Info" value="Update" type="submit" class="btn btn-danger">Update</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="changepass">

              <?php 
              $UserID = $_SESSION['Doctor']['TeachingID'];
              $Doctor_Data = "SELECT * FROM user WHERE UserID = '$UserID'";
              $data = mysqli_fetch_row($conn->query($Doctor_Data));
              //pr($data);

              ?>
                <form  class="form-horizontal" method="POST" action="update_pass.php">
                  
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Password</label>

                    <div class="col-sm-10">
                      <input name="oldpass" type="password"  class="form-control" id="inputName" placeholder="Enter Old password">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Password</label>

                    <div class="col-sm-10">
                      <input name="pass" type="password"  class="form-control" id="inputName" placeholder="Enter New password">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button  name="Update_Info" type="submit" class="btn btn-danger">Update</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->


            </div>
            <!-- /.tab-content -->
          </div>
          
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>


        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; CMP-Notifier 2015-2016 <a href="#">CMP Notifier</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>         
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->



          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

  <?php include('scripts.php');?>    
  </body>
</html>
